// export const logorus = require('./Логотип.png')
// export const hyunday250jpg = require('./hyunday-250.jpg')
// export const hyunday250png = require('./hyunday-250.png')
// export const logo = require('./logo.png')
// export const manipulyator = require('./manipulyator.png')
// export const minHyunday250png = require('./min-hyunday-250.png')
// export const minManipulyator = require('./min-manipulyator.png')
// export const minPogruzchik = require('./min-pogruzchik.png')
// export const minPolivochnyeMashiny = require('./min-polivochnye-mashiny.png')
// export const minSHACMAN = require('./min-SHACMAN.png')
// export const minShantui32 = require('./min-shantui-sd32.png')
// export const pogruzchik = require('./pogruzchik.png')
// export const polivochnyeMashiny = require('./polivochnye-mashiny.png')
// export const SHACMAN = require('./SHACMAN.png')
// export const shantui32 = require('./min-shantui-sd32.png')
// export const jdeerjpg = require('./john_deere744K.jpg')
// export const jdeerpng = require('./john_deere744K.png')
// export const phonegif = require('./phone.gif')
// export const contactsIcon = require('./contacts-icon.png')
// export const glavnayaIcon = require('./glavnaya.png')
// export const graficIcon = require('./grafik-icon.png')
// export const techsIcon = require('./techs-icon.png')
// export const workIcon = require('./work-icon.png')

import logorus from './Логотип.png'
import hyunday250jpg from './hyunday-250.jpg'
import hyunday250png from './hyunday-250.png'
import logo from './logo.png'
import manipulyator from './manipulyator.png'
import minHyunday250png from './min-hyunday-250.png'
import minManipulyator from './min-manipulyator.png'
import minPogruzchik from './min-pogruzchik.png'
import minPolivochnyeMashiny from './min-polivochnye-mashiny.png'
import minSHACMAN from './min-SHACMAN.png'
import minShantui32 from './min-shantui-sd32.png'
import pogruzchik from './pogruzchik.png'
import polivochnyeMashiny from './polivochnye-mashiny.png'
import SHACMAN from './SHACMAN.png'
import shantui32 from './min-shantui-sd32.png'
import jdeerjpg from './john_deere744K.jpg'
import jdeerpng from './john_deere744K.png'
import phonegif from './phone.gif'
import contactsIcon from './contacts-icon.png'
import glavnayaIcon from './glavnaya.png'
import graficIcon from './grafik-icon.png'
import techsIcon from './techs-icon.png'
import workIcon from './work-icon.png'

export {
	logorus,
	hyunday250jpg,
	hyunday250png,
	logo,
	manipulyator,
	minHyunday250png,
	minManipulyator,
	minPogruzchik,
	minPolivochnyeMashiny,
	minSHACMAN,
	minShantui32,
	pogruzchik,
	polivochnyeMashiny,
	SHACMAN,
	shantui32,
	jdeerjpg,
	jdeerpng,
	phonegif,
	contactsIcon,
	glavnayaIcon,
	graficIcon,
	techsIcon,
	workIcon,
}
