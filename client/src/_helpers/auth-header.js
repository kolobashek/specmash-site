export const authHeader = (isPost) => {
	// return authorization header with jwt token
	const currentUser = JSON.parse(localStorage.getItem('currentUser'))
	if (currentUser && currentUser.token && !isPost) {
		return { Authorization: `Bearer ${currentUser.token}` }
	} else if (currentUser && currentUser.token && isPost) {
		return {
			Authorization: `Bearer ${currentUser.token}`,
			'Content-Type': 'application/json',
		}
	} else {
		return {}
	}
}
