import React from 'react'
import { Link } from 'react-router-dom'
import { Maincontent } from 'pages/SpecmashPage/uikit'
import { minPogruzchik, minSHACMAN, minHyunday250png, minManipulyator, minPolivochnyeMashiny, minShantui32 } from 'img'

const Catalog = () => {
	return (
		<div className="content">
			<div className="catalog">
				<div className="catalog-item catalog-porgruzchiki">
					<div className="catalog-img-wrp">
						<img src={minPogruzchik} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Фронтальные погрузчики</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/wheelloaders">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
				<div className="catalog-item catalog-samosvaly">
					<div className="catalog-img-wrp">
						<img src={minSHACMAN} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Самосвалы</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/tippers">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
				<div className="catalog-item catalog-manipulyatory">
					<div className="catalog-img-wrp">
						<img src={minManipulyator} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Краны манипуляторы</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/manipulators">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
				<div className="catalog-item catalog-excovatory">
					<div className="catalog-img-wrp">
						<img src={minHyunday250png} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Экскаваторы</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/excavators">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
				<div className="catalog-item catalog-polivochnye">
					<div className="catalog-img-wrp">
						<img src={minPolivochnyeMashiny} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Поливочные машины</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/wateringmachines">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
				<div className="catalog-item catalog-buldozery">
					<div className="catalog-img-wrp">
						<img src={minShantui32} alt="" />
					</div>
					<div className="cat-h3">
						<h3>Бульдозеры</h3>
					</div>
					<div className="but-catalog-wrp">
						<Link to="/bulldozers">
							<button className="but-catalog">Подробнее</button>
						</Link>
					</div>
				</div>
			</div>
			<Maincontent />
		</div>
	)
}

export { Catalog }
