import React, { useState } from 'react'
import { Tabs, Tab, Spinner } from 'react-bootstrap'
import { Formik, Field, Form, ErrorMessage, useFormik } from 'formik'
import * as Yup from 'yup'
import InputMask from 'react-input-mask'
import { useAuth } from 'src/_services'
import { useHistory } from 'react-router-dom'

const LoginPage = () => {
	const history = useHistory()
	const auth = useAuth()
	const [activeKey, setActiveKey] = useState('login')
	// redirect to home if already logged in
	if (auth.user) {
		history.push('/org')
	}

	const fetchSubmit = (values, { setStatus, setSubmitting }) => {
		auth.login(values.username, values.password).then(
			() => {
				setSubmitting(false)
				history.goBack()
			},
			(error) => {
				setSubmitting(false)
				setStatus(error)
			}
		)
	}
	const regSubmit = ({ username, password, firstname, lastname, email, phone }, { setStatus, setSubmitting }) => {
		setStatus()
		auth.register({
			username,
			password,
			firstname,
			lastname,
			email,
			phone,
		}).then(
			() => {
				history.goBack()
			},
			(error) => {
				setSubmitting(false)
				setStatus(error)
			}
		)
	}
	const signup = useFormik({
		initialValues: {
			username: '',
			password: '',
			firstname: '',
			lastname: '',
			email: '',
			confirmPassword: '',
			phone: '',
		},
		validationSchema: Yup.object().shape({
			username: Yup.string().required('Введите имя пользователя'),
			password: Yup.string().min(6, 'Пароль должен быть длиннее 6 символов').required('Введите пароль'),
			firstname: Yup.string().required('Введите имя'),
			lastname: Yup.string().required('Введите фамилию'),
			email: Yup.string().email('Некорректный адрес электронной почты'),
			confirmPassword: Yup.string()
				.oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
				.required('Введите пароль повторно'),
			phone: Yup.string().matches(phoneRegExp, 'Некорректный номер телефона').required('Phone is required'),
		}),
		onSubmit: ({ username, password, firstname, lastname, email, phone }, { setStatus, setSubmitting }) => {
			setStatus()
			auth.register({
				username,
				password,
				firstname,
				lastname,
				email,
				phone,
			}).then(
				() => {
					history.goBack()
				},
				(error) => {
					setSubmitting(false)
					setStatus(error)
				}
			)
		},
	})
	const signin = useFormik({
		initialValues: {
			username: '',
			password: '',
		},
		validationSchema: Yup.object().shape({
			username: Yup.string().required('Введите имя пользователя'),
			password: Yup.string().required('Введите пароль'),
		}),
		onSubmit: (values, andsome) => fetchSubmit(values, andsome),
	})
	const phoneRegExp = /[+][0-9]\([0-9]{3}\)[ ][0-9]{3}[-][0-9]{4}/
	const phoneNumberMask = '+7(999) 999-9999'
	return (
		<div className="content-login">
			<Tabs className="tab-cont" activeKey={activeKey} onSelect={(key) => setActiveKey(key)}>
				<Tab eventKey="login" title="Вход">
					<form onSubmit={signin.handleSubmit}>
						<div className="form-group">
							<label htmlFor="username">Имя пользователя</label>
							<input
								id="username"
								name="username"
								type="text"
								onChange={signin.handleChange}
								value={signin.values.username}
								className={
									'form-control' +
									(signin.errors.username && signin.touched.username ? ' is-invalid' : '')
								}
							/>
							{signin.errors.username ? <div>{signin.errors.username}</div> : null}
						</div>
						<div className="form-group">
							<label htmlFor="password">Пароль</label>
							<input
								id="password"
								name="password"
								type="password"
								onChange={signin.handleChange}
								value={signin.values.password}
								className={
									'form-control' +
									(signin.errors.password && signin.touched.password ? ' is-invalid' : '')
								}
							/>
							{signin.errors.password ? <div>{signin.errors.password}</div> : null}
						</div>
						<div className="form-group">
							{!signin.isSubmitting && (
								<>
									<button type="submit" className="btn btn-primary" disabled={signin.isSubmitting}>
										Войти
									</button>
									<a
										onClick={() =>
											this.setState({
												activeKey: 'register',
											})
										}
									>
										<u>или зарегистрироваться</u>
									</a>
								</>
							)}
							{signin.isSubmitting && (
								<Spinner animation="border" role="status">
									<span className="sr-only">Loading...</span>
								</Spinner>
							)}
						</div>
						{status && <div className="alert alert-danger">{status}</div>}
					</form>
				</Tab>
				<Tab eventKey="register" title="Регистрация">
					<Formik
						initialValues={{
							username: '',
							password: '',
							firstname: '',
							lastname: '',
							email: '',
							confirmPassword: '',
							phone: '',
						}}
						validationSchema={Yup.object().shape({
							username: Yup.string().required('Введите имя пользователя'),
							password: Yup.string()
								.min(6, 'Пароль должен быть длиннее 6 символов')
								.required('Введите пароль'),
							firstname: Yup.string().required('Введите имя'),
							lastname: Yup.string().required('Введите фамилию'),
							email: Yup.string().email('Некорректный адрес электронной почты'),
							confirmPassword: Yup.string()
								.oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
								.required('Введите пароль повторно'),
							phone: Yup.string()
								.matches(phoneRegExp, 'Некорректный номер телефона')
								.required('Phone is required'),
						})}
						onSubmit={regSubmit}
					>
						{({ errors, status, touched, isSubmitting }) => (
							<Form>
								<div className="form-group">
									<label htmlFor="username">Имя пользователя</label>
									<Field
										name="username"
										type="text"
										className={
											'form-control' + (errors.username && touched.username ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="username" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="password">Пароль</label>
									<Field
										name="password"
										type="password"
										className={
											'form-control' + (errors.password && touched.password ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="password" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="confirmPassword">Подтвердите пароль</label>
									<Field
										name="confirmPassword"
										type="password"
										className={
											'form-control' +
											(errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="firstname">Имя</label>
									<Field
										name="firstname"
										type="text"
										className={
											'form-control' +
											(errors.firstname && touched.firstname ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="firstname" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="lastname">Фамилия</label>
									<Field
										name="lastname"
										type="text"
										className={
											'form-control' + (errors.lastname && touched.lastname ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="lastname" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="phone">Телефон</label>
									<Field
										name="phone"
										type="tel"
										className={
											'form-control' + (errors.phone && touched.phone ? ' is-invalid' : '')
										}
									>
										{({ field }) => (
											<InputMask
												{...field}
												className={
													'form-control' +
													(errors.phone && touched.phone ? ' is-invalid' : '')
												}
												mask={phoneNumberMask}
												type="tel"
											/>
										)}
									</Field>
									<ErrorMessage name="phone" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="email">Email</label>
									<Field
										name="email"
										type="email"
										className={
											'form-control' + (errors.email && touched.email ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="email" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									{!isSubmitting && (
										<button type="submit" className="btn btn-primary" disabled={isSubmitting}>
											Зарегистрироваться
										</button>
									)}
									{isSubmitting && (
										<div className="preloader">
											<Spinner animation="border" role="status">
												<span className="sr-only">Loading...</span>
											</Spinner>
										</div>
									)}
								</div>
								{status && <div className={'alert alert-danger'}>{status}</div>}
							</Form>
						)}
					</Formik>
				</Tab>
			</Tabs>
		</div>
	)
}

export { LoginPage }
