import React, { Component } from 'react'
import { techService } from 'src/_services'
import 'pages/OrganizationPage/TablePage/components/Grafik-styles.scss'

// TODO convert to a pure function
class Techs extends Component {
	constructor(props) {
		super(props)

		this.state = {
			techs: [],
			model: '',
			editingTech: null,
			bort: '',
			govNumber: '',
			nickname: '',
			edit_model: '',
			edit_bort: '',
			edit_govNumber: '',
			edit_nickname: '',
			show_deleted: false,
		}
	}

	componentDidMount() {
		this.getTechnics()
	}
	showDeleted(show_deleted) {
		this.setState({ show_deleted }, () => this.getTechnics())
	}
	getTechnics = () => {
		this.state.show_deleted
			? techService.getAll().then((techs) => {
					// const activeTechs = techs.filter(tech => {
					// 	return tech.status
					// })
					if (techs) this.setState({ techs, editingTech: null })
			  })
			: techService.getAllActive().then((techs) => {
					// const activeTechs = techs.filter(tech => {
					// 	return tech.status
					// })
					if (techs) this.setState({ techs, editingTech: null })
			  })
	}

	addTechnics = () => {
		const { bort, model, nickname, govNumber } = this.state
		// fetch(`http://localhost:4000/technics/add?model=${model}&bort=${bort}&nickname=${nickname}`)
		// 	.then(this.getTechnics())
		// 	.then(this.setState({ model: '', bort: '', nickname: '' }))
		// 	.catch(err => {throw err})
		techService
			.add({ bort, model, nickname, govNumber })
			.then(() => this.getTechnics())
			.then(
				this.setState({
					model: '',
					bort: '',
					nickname: '',
					govNumber: '',
				})
			)
			.catch((err) => {
				throw err
			})
	}

	deleteTechnic = (id, status) => {
		techService
			.del({ id, status })
			.then(() => this.getTechnics())
			.catch((err) => {
				throw err
			})
	}

	editBut = (id, edit_bort, edit_model, edit_nickname, edit_govNumber) => {
		this.setState({
			editingTech: id,
			edit_model,
			edit_bort,
			edit_nickname,
			edit_govNumber,
		})
	}

	editComplete = (id, bort, model, nickname, govNumber) => {
		techService.update({ id, bort, model, nickname, govNumber }).then((resp) => {
			if (resp) {
				this.setState({
					edit_model: '',
					edit_bort: '',
					edit_nickname: '',
					edit_govNumber: '',
				})
				this.getTechnics()
			}
		})
	}

	inputHandler = (name, value) => {
		this.setState({ [name]: value })
	}

	renderTech = ({ id, model, bort, govNumber, nickname, status }) => {
		const activeClass = !status ? 'not-active' : 'active'
		const { editingTech, edit_model, edit_bort, edit_nickname, edit_govNumber } = this.state
		if (editingTech !== id) {
			return (
				<li key={id} className={activeClass}>
					{bort} - {model} &quot;{nickname}&quot; {govNumber}
					<div>
						<button className="edit-but" onClick={() => this.editBut(id, bort, model, nickname, govNumber)}>
							&#9998;
						</button>
						<button
							className="del-but"
							onClick={() => {
								return this.deleteTechnic(id, status)
							}}
						>
							X
						</button>
					</div>
				</li>
			)
		} else {
			return (
				<form
					key={id}
					onSubmit={(e) => {
						this.editComplete(id, edit_bort, edit_model, edit_nickname, edit_govNumber)
						e.preventDefault()
					}}
				>
					<li>
						<input
							onChange={(e) => this.inputHandler('edit_bort', e.target.value)}
							value={edit_bort}
							placeholder="Бортовой номер"
						/>
						<input
							onChange={(e) => this.inputHandler('edit_model', e.target.value)}
							value={edit_model}
							placeholder="Модель/название техники"
						/>
						<input
							onChange={(e) => this.inputHandler('edit_nickname', e.target.value)}
							value={edit_nickname}
							placeholder="Кодовое имя"
						/>
						<input
							onChange={(e) => this.inputHandler('edit_govNumber', e.target.value)}
							value={edit_govNumber}
							placeholder="Гос. номер"
						/>
						<button
							className="add-but"
							disabled={!edit_model && !edit_bort && !edit_nickname && !edit_govNumber}
							type="submit"
						>
							&#10004;
						</button>
						<button className="del-but" onClick={() => this.setState({ editingTech: null })}>
							X
						</button>
					</li>
				</form>
			)
		}
	}

	render() {
		const { techs, model, bort, nickname, govNumber, show_deleted } = this.state
		return (
			<div className="wrapper-ul">
				<ul>
					<li onClick={() => this.showDeleted(!show_deleted)}>
						<div>
							<input
								type="checkbox"
								checked={show_deleted}
								onChange={() => this.showDeleted(!show_deleted)}
								name={'showdeleted'}
							/>
							<label htmlFor="showdeleted">Показать удаленные</label>
						</div>
					</li>
					<li>
						<input
							onChange={(e) => this.inputHandler('bort', e.target.value)}
							value={bort}
							placeholder="Бортовой номер"
						/>
						<input
							onChange={(e) => this.inputHandler('model', e.target.value)}
							value={model}
							placeholder="Модель/название техники"
						/>
						<input
							onChange={(e) => this.inputHandler('nickname', e.target.value)}
							value={nickname}
							placeholder="Кодовое имя"
						/>
						<input
							onChange={(e) => this.inputHandler('govNumber', e.target.value)}
							value={govNumber}
							placeholder="Гос. номер"
						/>
						<button
							className="add-but"
							disabled={!model && !bort && !nickname}
							onClick={() => this.addTechnics()}
						>
							+
						</button>
					</li>
					{techs.map(this.renderTech)}
				</ul>
			</div>
		)
	}
}

export { Techs }
