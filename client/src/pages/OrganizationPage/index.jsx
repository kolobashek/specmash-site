import React from 'react'
import {
	BrowserRouter as Router,
	Route,
	useLocation,
	useRouteMatch,
} from 'react-router-dom'
// import { Role } from 'src/_helpers'
// import { TableScreen, Techs, User, Units } from 'pages/OrganizationPage'
import { PrivateRoute } from 'src/_components'
import { TableScreen, Techs, User, Units, AdminScreen, Objects } from 'pages'
import { LoginPage } from 'src/pages/LoginPage'
import { NavMenu } from 'src/pages/OrganizationPage/uikit'
// import { authenticationService } from 'src/_services'

////////////////////////////////////////////////////////////
// 1. Click the public page
// 2. Click the protected page
// 3. Log in
// 4. Click the back button, note the URL each time

const Organization = () => {
	let { path } = useRouteMatch()
	return (
		<Router>
			<NavMenu />
			{/* <PrivateRoute
          path={`${path}/adminsait`}
          component={Adminsait}
        /> */}
			<PrivateRoute path={`${path}/`} exact>
				<TableScreen />
			</PrivateRoute>
			<PrivateRoute path={`${path}/user`}>
				<User />
			</PrivateRoute>
			<PrivateRoute path={`${path}/techs`}>
				<Techs />
			</PrivateRoute>
			<PrivateRoute path={`${path}/objects`}>
				<Objects />
			</PrivateRoute>
			<PrivateRoute path={`${path}/units`}>
				<Units />
			</PrivateRoute>
			<PrivateRoute path={`${path}/admin`}>
				<AdminScreen />
			</PrivateRoute>
			<Route path={`/login`}>
				<LoginPage />
			</Route>
		</Router>
	)
}

export { Organization }
