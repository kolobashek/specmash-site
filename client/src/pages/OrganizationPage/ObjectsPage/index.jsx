import React, { useEffect, useState } from 'react'
import { objectService } from 'src/_services'
import 'pages/OrganizationPage/TablePage/components/Grafik-styles.scss'

const Objects = () => {
	const [objects, setobjects] = useState([])
	// const [name, setname] = useState('')
	const [comment, setcomment] = useState('')
	const [editingObject, seteditingObject] = useState(null)
	const [edit_name, setedit_name] = useState('')
	const [edit_comment, setedit_comment] = useState('')
	const [show_deleted, setshow_deleted] = useState(false)

	useEffect(() => {
		getObjects()
	}, [show_deleted])
	const showDeleted = (value) => {
		setshow_deleted(value)
	}
	const getObjects = () => {
		show_deleted
			? objectService.getAll().then((items) => {
					// const activeObjects = objects.filter(tech => {
					// 	return tech.status
					// })
					if (items) {
						setobjects(items)
						seteditingObject(null)
					}
			  })
			: objectService.getAllActive().then((items) => {
					// const activeObjects = objects.filter(tech => {
					// 	return tech.status
					// })
					if (items) {
						setobjects(items)
						seteditingObject(null)
					}
			  })
	}

	const addObjects = () => {
		// fetch(`http://localhost:4000/Objects/add?model=${model}&bort=${bort}&nickname=${nickname}`)
		// 	.then(this.getObjects())
		// 	.then(this.setState({ model: '', bort: '', nickname: '' }))
		// 	.catch(err => {throw err})
		objectService
			.add({ name, comment })
			.then(() => getObjects())
			.then(() => {
				setname('')
				setcomment('')
			})
			.catch((err) => {
				throw err
			})
	}

	const deleteObject = (id, status) => {
		objectService
			.del({ id, status })
			.then(() => getObjects())
			.catch((err) => {
				throw err
			})
	}

	const editBut = (id, edit_name_value, edit_comment_value) => {
		seteditingObject(id)
		setedit_name(edit_name_value)
		setedit_comment(edit_comment_value)
	}

	const editComplete = (id, name_value, comment_value) => {
		objectService.update({ id, name: name_value, comment: comment_value }).then((resp) => {
			if (resp) {
				setname('')
				setcomment('')
				getObjects()
			}
		})
	}

	const inputHandler = (name, value) => {
		switch (name) {
			case 'edit_name':
				setedit_name(value)
				break
			case 'edit_comment':
				setedit_comment(value)
				break

			default:
				console.warn(name, value)
		}
	}

	const renderObjects = ({ id, name, comment, status }) => {
		const activeClass = !status ? 'not-active' : 'active'
		if (editingObject !== id) {
			return (
				<li key={id} className={activeClass}>
					{name} - {comment}
					<div>
						<button className="edit-but" onClick={() => editBut(id, name, comment)}>
							&#9998;
						</button>
						<button
							className="del-but"
							onClick={() => {
								return deleteObject(id, status)
							}}
						>
							X
						</button>
					</div>
				</li>
			)
		} else {
			return (
				<form
					key={id}
					onSubmit={(e) => {
						editComplete(id, edit_name, edit_comment)
						e.preventDefault()
					}}
				>
					<li>
						<input
							onChange={(e) => inputHandler('edit_name', e.target.value)}
							value={edit_name}
							placeholder="Название объекта"
						/>
						<input
							onChange={(e) => inputHandler('edit_comment', e.target.value)}
							value={edit_comment}
							placeholder="Комментарий"
						/>
						<button className="add-but" disabled={!edit_name && !edit_comment} type="submit">
							&#10004;
						</button>
						<button className="del-but" onClick={() => this.setState({ editingObject: null })}>
							X
						</button>
					</li>
				</form>
			)
		}
	}
	return (
		<div className="wrapper-ul">
			<ul>
				<li onClick={() => showDeleted(!show_deleted)}>
					<div>
						<input
							type="checkbox"
							checked={show_deleted}
							onChange={() => showDeleted(!show_deleted)}
							name={'showdeleted'}
						/>
						<label htmlFor="showdeleted">Показать удаленные</label>
					</div>
				</li>
				<li>
					<input
						onChange={(e) => inputHandler('name', e.target.value)}
						value={name}
						placeholder="Название объекта"
					/>
					<input
						onChange={(e) => inputHandler('comment', e.target.value)}
						value={comment}
						placeholder="Комментарий"
					/>
					<button className="add-but" disabled={!name && !comment} onClick={() => addObjects()}>
						+
					</button>
				</li>
				{objects.map(renderObjects)}
			</ul>
		</div>
	)
}

export { Objects }
