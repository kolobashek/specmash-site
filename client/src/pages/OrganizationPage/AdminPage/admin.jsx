import React, { useState, useEffect } from 'react'
import update from 'react-addons-update'
import { Role } from 'src/_helpers'
import { userService } from 'src/_services'

export function AdminScreen() {
	const [users, setUsers] = useState([])

	async function getUsers() {
		const getusers = await userService.getAll()
		setUsers(getusers)
	}

	const roleUpdate = async (e, user) => {
		const updatedUser = await userService.update({
			id: user.id,
			role: e.target.value,
		})
		getUsers()
		return updatedUser
	}

	// useEffect(() => {
	// 	getUsers()
	// }, [])

	return (
		<div className="wrapper-ul">
			<ul>
				{users.map((user) => {
					const activeClass = !user.status ? 'not-active' : 'active'
					const currentUserUsername = localStorage.currentUser
						? localStorage.currentUser.username
						: null
					if (
						user.username !== 'kolobashek' &&
						user.username !== 'китай' &&
						user.username !== currentUserUsername
					)
						return (
							<li key={user.id} className={activeClass}>
								<span>
									{user.lastname} {user.firstname}{' '}
									{user.username} {user.phone} {user.email}{' '}
								</span>
								<select
									value={user.role || 0}
									onChange={(e) => roleUpdate(e, user)}
								>
									<option value={0}>-</option>
									{Object.keys(Role).map((role, key) => {
										return (
											<option key={key} value={role}>
												{' '}
												{role}{' '}
											</option>
										)
									})}
								</select>
							</li>
						)
				})}
			</ul>
		</div>
	)
}
