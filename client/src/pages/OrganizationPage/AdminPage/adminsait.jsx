// import React, { Component } from 'react'
// import { Modal, Button } from 'react-bootstrap'
// import { AdminContent } from 'src/pages/OrganizationPage/uikit'
// import { AddTechForm } from 'src/pages/OrganizationPage/uikit'
// import { advtechService } from 'src/_services'

// class Adminsait extends Component {
// 	constructor(props) {
// 		super(props)

// 		this.state = {
// 			modalShow: false,
// 			advTechs: [],
// 			techTypes: [
// 				{
// 					name: 'Погрузчики',
// 					value: 'wheelloaders',
// 				},
// 				{
// 					name: 'Экскаваторы',
// 					value: 'excavators',
// 				},
// 				{
// 					name: 'Бульдозеры',
// 					value: 'bulldozers',
// 				},
// 				{
// 					name: 'Поливочные машины',
// 					value: 'wateringmachines',
// 				},
// 				{
// 					name: 'Воровайки',
// 					value: 'manipulators',
// 				},
// 				{
// 					name: 'Самосвалы',
// 					value: 'tippers',
// 				},
// 			],
// 			currentValue: '',
// 		}
// 	}
// 	async componentDidMount() {
// 		this.getTechs()
// 	}
// 	getTechs = async (param) => {
// 		try {
// 			const advTechs = await advtechService.getAll(param)
// 			this.setState({ advTechs })
// 		} catch (error) {
// 			console.warn(error)
// 		}
// 	}
// 	handleClose = () => {
// 		this.setState({ modalShow: false })
// 	}
// 	showModal = () => {
// 		this.setState({ modalShow: true })
// 	}
// 	render() {
// 		return (
// 			<>
// 				<div className="top-select">
// 					<Button onClick={this.showModal}>Добавить</Button>
// 					<div className="select-adminsait">
// 						<select name="" value={this.state.currentValue}>
// 							{this.state.techTypes.map((item, key) => {
// 								return (
// 									<option value={item.value} key={key}>
// 										{item.name}
// 									</option>
// 								)
// 							})}
// 						</select>
// 					</div>
// 				</div>
// 				<div className="admin-content">
// 					<div className="admin-content-wrp">
// 						{this.state.advTechs.length > 0 &&
// 							this.state.advTechs.map((item, key) => {
// 								return <AdminContent key={key} tech={item} />
// 							})}
// 					</div>
// 				</div>
// 				<Modal show={this.state.modalShow} onHide={this.handleClose}>
// 					<Modal.Header closeButton>
// 						<Modal.Title>Modal heading</Modal.Title>
// 					</Modal.Header>
// 					<Modal.Body>
// 						<AddTechForm />
// 					</Modal.Body>
// 					<Modal.Footer>
// 						<Button variant="secondary" onClick={this.handleClose}>
// 							Отменить
// 						</Button>
// 						<Button
// 							variant="primary"
// 							onClick={() => this.handleClose('save')}
// 						>
// 							Сохранить
// 						</Button>
// 					</Modal.Footer>
// 				</Modal>
// 			</>
// 		)
// 	}
// }

// export { Adminsait }
