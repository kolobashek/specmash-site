import React, { useEffect, useState } from 'react'
import StatefulMultiSelect from '@khanacademy/react-multi-select'
import { techService, unitService, objectService, useAuth } from 'src/_services'
import './Grafik-styles.scss'

const Filter = ({ handler }) => {
	const dateNow = Date.now() - (Date.now() % (1000 * 3600 * 24))
	let dateNowFormatted = new Date(dateNow)
	dateNowFormatted = dateNowFormatted.toISOString().substr(0, 10)
	const [date, setdate] = useState(dateNow)
	const [dateFormatted, setdateFormatted] = useState(dateNowFormatted)
	const [dateRange, setdateRange] = useState(3)
	const [datePeriod, setdatePeriod] = useState('+')
	// const [dateArr, setdateArr] = useState([])
	// const [unixDateArr, setunixDateArr] = useState([])
	const [techs, settechs] = useState([])
	const [techOptions, settechOptions] = useState([])
	const [techOptionsSelected, settechOptionsSelected] = useState([])
	const [units, setunits] = useState([])
	const [unitsOptions, setunitsOptions] = useState([])
	// const [unitsSelected, setunitsSelected] = useState([])
	const [unitsOptionsSelected, setunitsOptionsSelected] = useState([])
	const [objects, setobjects] = useState([])
	const [objectsOptions, setobjectsOptions] = useState([])
	// const [objectsSelected, setobjectsSelected] = useState([])
	const [objectsOptionsSelected, setobjectsOptionsSelected] = useState([])
	const { isAdmin } = useAuth()

	useEffect(() => {
		techService
			.getAllActive()
			.then((response) => {
				settechOptions(
					response.map((item) => ({
						label: item.nickname,
						value: item.id,
					}))
				)
				settechOptionsSelected(response.map((item) => item.id))
				settechs(response)
			})
			.catch((err) => {
				throw err
			})
		unitService
			.getAll()
			.then((response) => {
				setunitsOptions(
					response.map((item) => ({
						label: item.last_name,
						value: item.id,
					}))
				)
				setunitsOptionsSelected(response.map((item) => item.id))
				setunits(response)
			})
			.catch((err) => {
				throw err
			})
		objectService.getAllActive().then((response) => {
			setobjectsOptions(
				response.map((item) => ({
					label: item.name,
					value: item.id,
				}))
			)
			setobjectsOptionsSelected(response.map((item) => item.id))
			setobjects(response)
		})
	}, [])
	useEffect(() => {
		filter(techs, 'id', techOptionsSelected, 'techs')
	}, [techs])
	useEffect(() => {
		filter(units, 'id', unitsOptionsSelected, 'unitsSelected')
	}, [unitsOptionsSelected])
	useEffect(() => {
		filter(objects, 'id', objectsOptionsSelected, 'objectsSelected')
	}, [objectsOptionsSelected])
	useEffect(() => {
		handler('units', units)
	}, [units])
	useEffect(() => {
		handler('objects', objects)
	}, [objects])
	useEffect(() => {
		getDateArray()
	}, [date, dateRange, datePeriod, dateFormatted])

	const getDateArray = () => {
		let a = date
		const day = 3600 * 24 * 1000
		let unixDateArr1 = [date]
		if (datePeriod === '+') {
			for (let i = 0; i < dateRange; i++) {
				a = a + day
				unixDateArr1.push(a)
			}
		} else if (datePeriod === '-') {
			for (let i = 0; i < dateRange; i++) {
				a = a - day
				unixDateArr1.unshift(a)
			}
		} else if (datePeriod === '+-') {
			for (let i = 0; i < dateRange; i++) {
				a = a - day
				unixDateArr1.unshift(a)
			}
			a = date
			for (let i = 0; i < dateRange; i++) {
				a = a + day
				unixDateArr1.push(a)
			}
		}
		handler('unixDateArr', unixDateArr1)
	}

	const dateChange = (e) => {
		setdate(Date.parse(e.target.value))
		setdateFormatted(e.target.value)
	}

	const datePeriodSwitch = () => {
		if (datePeriod === '+') {
			setdatePeriod('-')
		} else if (datePeriod === '-') {
			setdatePeriod('+-')
		} else setdatePeriod('+')
	}

	const dateRangeChange = (e) => {
		const { value, maxLength } = e.target
		setdateRange(value.slice(0, maxLength))
	}

	const filter = (arr, idname, ids, name) => {
		const filteredArray = arr.filter((el) => {
			return ~ids.indexOf(el[idname])
		})
		handler(name, filteredArray)
	}

	return (
		<div className="header">
			<div className="first-line">
				{isAdmin && (
					<div className="input-date">
						<input
							type="date"
							onChange={dateChange}
							value={dateFormatted}
						/>
					</div>
				)}

				{isAdmin && (
					<div className="days">
						<button onClick={datePeriodSwitch}>{datePeriod}</button>
						<input
							type="number"
							maxLength="3"
							min="0"
							max="999"
							value={dateRange}
							onChange={dateRangeChange}
						/>
					</div>
				)}

				<div className="filter1">
					<StatefulMultiSelect
						options={objectsOptions}
						selected={objectsOptionsSelected}
						onSelectedChanged={setobjectsOptionsSelected}
						placeholderButtonLabel="Выберите объект"
						overrideStrings={{
							selectSomeItems: 'Выберите объекты',
							allItemsAreSelected: 'Все объекты выбраны',
							selectAll: 'Выбрать все',
							search: 'Найти',
						}}
					/>
				</div>
				<div className="filter1">
					<StatefulMultiSelect
						options={techOptions}
						selected={techOptionsSelected}
						onSelectedChanged={settechOptionsSelected}
						placeholderButtonLabel="Выберите технику"
						overrideStrings={{
							selectSomeItems: 'Выберите технику',
							allItemsAreSelected: 'Вся техника выбрана',
							selectAll: 'Выбрать всё',
							search: 'Поиск',
						}}
					/>
				</div>

				<div className="filter1" disabled>
					<StatefulMultiSelect
						disabled
						options={unitsOptions}
						selected={unitsOptionsSelected}
						onSelectedChanged={setunitsOptionsSelected}
						placeholderButtonLabel="Выберите работников"
						overrideStrings={{
							selectSomeItems: 'Выберите работников',
							allItemsAreSelected: 'Все работники выбраны',
							selectAll: 'Выбрать всех',
							search: 'Поиск',
						}}
					/>
				</div>
			</div>
		</div>
	)
}

export { Filter }
