import React, { useEffect, useState } from 'react'
// import PropTypes from 'prop-types'
import { shiftService, useAuth } from 'src/_services'
import './Grafik-styles.scss'
import ShiftCell from './shiftCell'

const Table = (props) => {
	// const [date, setDate] = useState(0)
	const [unixDateArr, setUnixDateArr] = useState([])
	const [techs, setTechs] = useState([])
	const [shifts, setShifts] = useState([])
	const [units, setUnits] = useState([])
	// const [unitsSelected, setUnitsSelected] = useState([])
	const [objects, setObjects] = useState([])
	// const [objectsSelected, setObjectsSelected] = useState([])
	// const [modalShow, setModalShow] = useState(false)
	// const [comment, setComment] = useState('')
	// const [currentShift, setCurrentShift] = useState('')
	let { isAdmin } = useAuth()

	useEffect(() => {
		setUnixDateArr(props.date.unixDateArr)
	}, [])
	useEffect(() => {
		getShifts()
	}, [unixDateArr])
	useEffect(() => {
		setUnixDateArr(props.date.unixDateArr)
	}, [props.date.unixDateArr])
	useEffect(() => {
		setTechs(props.techs)
	}, [props.techs])
	useEffect(() => {
		setUnits(props.units.units)
	}, [props.units.units])
	useEffect(() => {
		setObjects(props.objects)
	}, [props.objects])
	// useEffect(() => {
	// 	setUnitsSelected(props.units.unitsSelected)
	// }, [props.units.unitsSelected])

	// getShifts = (techs, units) => {
	const getShifts = () => {
		const dates = unixDateArr
		if (dates.length > 0) {
			const dateStart = dates[0]
			const dateEnd = dates[dates.length - 1]
			shiftService
				.getAll({ dateStart, dateEnd })
				.then((shiftsRecieved) => {
					if (shiftsRecieved) setShifts(shiftsRecieved)
				})
				.catch((err) => console.warn(err))
		}
	}
	const shiftHandler = (name, shift) => {
		if (shift.id) {
			shiftUpdate(shift)
		} else {
			shiftAdd(shift)
		}
	}
	const shiftAdd = ({ unit, tech, object, half, date }) => {
		return shiftService
			.add({ unit, tech, object, half, date })
			.then((shift) => {
				getShifts()
				return shift
			})
	}
	const shiftUpdate = (data) => {
		shiftService.update(data).then(() => getShifts())
	}

	const renderRow = (date, key) => {
		let dateFormatted = new Date(date)
		// dateFormatted = dateFormatted.toISOString().substr(0, 10)
		const options = {
			weekday: 'short',
			year: 'numeric',
			month: 'long',
			day: 'numeric',
		}
		dateFormatted = dateFormatted.toLocaleDateString('ru', options)
		return (
			<React.Fragment key={key}>
				<tr>
					<th>{dateFormatted}</th>
					<th>I</th>
					{isAdmin ? shiftRow('1', date) : shiftRowClosed('1', date)}
				</tr>
				<tr>
					<th className="empty"></th>
					<th>II</th>
					{isAdmin ? shiftRow('2', date) : shiftRowClosed('2', date)}
				</tr>
			</React.Fragment>
		)
	}
	const shiftRow = (shift_number, date) => {
		return techs.map((tech, key) => {
			const currentShift = shifts.find((shift) => {
				if (
					shift.date == date &&
					shift.half == shift_number &&
					shift.tech == tech.id
				)
					return shift
			})
			const none = {
				tech: tech.id,
				half: shift_number,
				date,
			}
			const shiftOrNone = currentShift || none
			return (
				<ShiftCell
					key={key}
					units={units}
					objects={objects}
					shift={shiftOrNone}
					shiftHandler={shiftHandler}
				/>
			)
		})
	}
	const shiftRowClosed = (shift_number, date) => {
		return techs.map((tech, key) => {
			const currentShift = shifts.find((shift) => {
				shift.date === date &&
					shift.half === shift_number &&
					shift.tech === tech.id
			})
			if (currentShift) {
				return (
					<td key={key}>
						{currentShift.unit ? (
							units.map((unit) => {
								if (
									unit.status ||
									unit.id === currentShift.unit
								) {
									return (
										<span key={unit.id}>
											{unit.last_name}
										</span>
									)
								}
								return
							})
						) : (
							<span>--</span>
						)}
					</td>
				)
			} else {
				return (
					<td key={key}>
						<span>--</span>
					</td>
				)
			}
		})
	}

	const renderTech = (item, key) => {
		return (
			<React.Fragment key={key}>
				<td>{`${item.bort} - ${item.nickname}`}</td>
			</React.Fragment>
		)
	}

	return (
		<div className="table">
			<table>
				<thead>
					<tr>
						<th className="top">Дата</th>
						<th className="top">Смена</th>
						{techs.map(renderTech)}
					</tr>
				</thead>
				<tbody>{unixDateArr.map(renderRow)}</tbody>
			</table>
		</div>
	)
}

export { Table }
