import React, { useEffect, useState } from 'react'
import { Modal, Button } from 'react-bootstrap'

const Comment = ({ show, shift, shiftHandler, modalHandler }) => {
	const [comment, setComment] = useState('')
	const [shower, setShower] = useState(show)
	useEffect(() => {
		setShower(show)
		setComment(shift.comment)
	}, [show, shift.comment])
	const handleClose = (save) => {
		if (save === 'save') {
			setShower(false)
			modalHandler(false)
			shiftHandler('comment', { ...shift, comment })
		}
		setShower(false)
		modalHandler(false)
	}
	return (
		<Modal show={shower} onHide={handleClose}>
			<Modal.Header closeButton>
				<Modal.Title>Modal heading</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<textarea
					placeholder="Напишите комментарий"
					value={comment}
					onChange={(e) => setComment(e.target.value)}
				/>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={handleClose}>
					Отменить
				</Button>
				<Button variant="primary" onClick={() => handleClose('save')}>
					Сохранить
				</Button>
			</Modal.Footer>
		</Modal>
	)
}

export { Comment }
