import React, { useState } from 'react'
import { FaCommentDots } from 'react-icons/fa'
import { FaRegCommentDots } from 'react-icons/fa'
import { Comment } from './commentModal'

const ShiftCell = ({ shiftHandler, units, objects, shift }) => {
	const [showComment, setShowComment] = useState(false)
	const commentStyle =
		shift.comment && shift.comment.length > 0 ? (
			<FaCommentDots className="comment-icon-full" size={30} />
		) : (
			<FaRegCommentDots className="comment-icon-empty" size={30} style={{ color: '#b0b7c0' }} />
		)
	const modalHandler = (show) => {
		setShowComment(show)
	}
	const UnitInput = () => (
		<select
			value={shift.unit || 0}
			onChange={(e) => {
				shift.unit = e.target.value || null
				shiftHandler('unit', shift)
			}}
		>
			<option value={0}>-</option>
			{units.map((unit) => {
				if (unit.status || unit.id === shift.unit) {
					return (
						<option key={unit.id} value={unit.id}>
							{unit.last_name}
							{unit.first_name && ` ${unit.first_name.slice(0, 1)}.`}
							{unit.middle_name && `${unit.middle_name.slice(0, 1)}.`}
						</option>
					)
				}
				return null
			})}
		</select>
	)
	const ObjectInput = () => (
		<select
			value={shift.object || 0}
			onChange={(e) => {
				shift.object = e.target.value || null
				shiftHandler('object', shift)
			}}
		>
			<option value={0}>-</option>
			{objects.objects.map((object) => {
				if (object.status || object.id === shift.object) {
					return (
						<option key={object.id} value={object.id}>
							{object.name}
						</option>
					)
				}
				return null
			})}
		</select>
	)
	return (
		<td>
			<div className="shiftCell">
				<div>
					<UnitInput />
					<ObjectInput />
				</div>
				<a onClick={() => setShowComment(true)}>{commentStyle}</a>
				<Comment shift={shift} show={showComment} shiftHandler={shiftHandler} modalHandler={modalHandler} />
			</div>
		</td>
	)
}

export default ShiftCell
