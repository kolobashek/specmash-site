import React, { useState } from 'react'
import { useAuth } from 'src/_services'
import { Table } from './components/table'
import { Filter } from './components/filter'

const TableScreen = () => {
	const [date, setDate] = useState(0)
	const [unixDateArr, setUnixDateArr] = useState([])
	const [techs, setTechs] = useState([])
	const [shifts, setShifts] = useState([])
	const [objects, setObjects] = useState([])
	const [objectsSelected, setobjectsSelected] = useState([])
	const [units, setUnits] = useState([])
	const [unitsSelected, setUnitsSelected] = useState([])
	const { isAdmin } = useAuth()

	const handler = (name, value) => {
		switch (name) {
			case 'date':
				setDate(value)
				break
			case 'unixDateArr':
				setUnixDateArr(value)
				break
			case 'techs':
				setTechs(value)
				break
			case 'shifts':
				setShifts(value)
				break
			case 'objects':
				setObjects(value)
				break
			case 'units':
				setUnits(value)
				break
			case 'unitsSelected':
				setUnitsSelected(value)
				break
			case 'objectsSelected':
				setobjectsSelected(value)
				break
			default:
				console.warn(name, value)
		}
	}
	return (
		<div className="tableScreenWrapper">
			<Filter handler={handler} />
			<Table
				shifts={shifts}
				techs={techs}
				objects={{ objects, objectsSelected }}
				units={{ units, unitsSelected }}
				date={{ date, unixDateArr }}
				admin={isAdmin}
			/>
		</div>
	)
}

export { TableScreen }
