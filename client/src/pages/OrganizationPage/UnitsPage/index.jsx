import React, { Component } from 'react'
import { unitService } from 'src/_services'
import 'pages/OrganizationPage/TablePage/components/Grafik-styles.scss'

// TODO convert to a pure component
class Units extends Component {
	constructor(props) {
		super(props)

		this.state = {
			units: [],
			editingUnit: null,
			first_name: '',
			middle_name: '',
			last_name: '',
			nickname: '',
			phone: '',
			edit_first_name: '',
			edit_middle_name: '',
			edit_last_name: '',
			edit_nickname: '',
			edit_phone: '',
		}
	}

	componentDidMount() {
		this.getUnits()
	}

	getUnits = () => {
		unitService
			.getAll()
			.then((units) => {
				// const activeUnits = units.filter(unit => {
				//     return unit.status
				// })
				if (units) this.setState({ units, editingUnit: null })
			})
			.catch((err) => {
				throw err
			})
	}

	addUnit = () => {
		const { first_name, middle_name, last_name, nickname, phone } =
			this.state
		unitService
			.add({ first_name, middle_name, last_name, nickname, phone })
			.then(() =>
				this.setState(
					{
						first_name: '',
						middle_name: '',
						last_name: '',
						nickname: '',
						phone: '',
					},
					() => this.getUnits()
				)
			)
			.catch((err) => {
				throw err
			})
	}

	deleteUnit = (id, status) => {
		unitService
			.del({ id, status })
			.then(() => this.getUnits())
			.catch((err) => {
				throw err
			})
	}

	editBut = (
		id,
		edit_first_name,
		edit_middle_name,
		edit_last_name,
		edit_nickname,
		edit_phone
	) => {
		this.setState({
			editingUnit: id,
			edit_last_name,
			edit_first_name,
			edit_middle_name,
			edit_nickname,
			edit_phone,
		})
	}

	editComplete = (
		last_name,
		first_name,
		middle_name,
		nickname,
		phone,
		id
	) => {
		unitService
			.update({ last_name, first_name, middle_name, nickname, phone, id })
			.then(() =>
				this.setState({ editingUnit: null }, () => this.getUnits())
			)
			.catch((err) => {
				throw err
			})
	}

	renderUnit = ({
		id,
		first_name,
		middle_name,
		last_name,
		nickname,
		phone,
		status,
	}) => {
		const activeClass = !status ? 'not-active' : 'active'
		const {
			editingUnit,
			edit_last_name,
			edit_first_name,
			edit_middle_name,
			edit_nickname,
			edit_phone,
		} = this.state
		if (editingUnit !== id) {
			return (
				<li key={id} className={activeClass}>
					<span>{`${last_name} ${first_name} ${middle_name} ${nickname && `"${nickname}"`} ${phone}`}</span>
					<div>
						<button
							className="edit-but"
							onClick={() => this.editBut(id, first_name, middle_name, last_name, nickname, phone)}
						>
							&#9998;
						</button>
						<button className="del-but" onClick={() => this.deleteUnit(id, status)}>
							X
						</button>
					</div>
				</li>
			)
		} else {
			return (
				<form
					key={id}
					onSubmit={(e) => {
						this.editComplete(
							edit_last_name,
							edit_first_name,
							edit_middle_name,
							edit_nickname,
							edit_phone,
							id
						)
						e.preventDefault()
					}}
				>
					<li>
						<input
							onChange={(e) =>
								this.inputHandler(
									'edit_last_name',
									e.target.value
								)
							}
							value={edit_last_name}
							placeholder="Фамилия работника"
							autoFocus
						/>
						<input
							onChange={(e) =>
								this.inputHandler(
									'edit_first_name',
									e.target.value
								)
							}
							value={edit_first_name}
							placeholder="Имя работника"
						/>
						<input
							onChange={(e) =>
								this.inputHandler(
									'edit_middle_name',
									e.target.value
								)
							}
							value={edit_middle_name}
							placeholder="Отчество работника"
						/>
						<input
							onChange={(e) =>
								this.inputHandler(
									'edit_nickname',
									e.target.value
								)
							}
							value={edit_nickname}
							placeholder="Прозвище"
						/>
						<input
							onChange={(e) =>
								this.inputHandler('edit_phone', e.target.value)
							}
							value={edit_phone}
							placeholder="Телефон"
						/>
						<button
							className="add-but"
							disabled={
								!edit_first_name &&
								!edit_middle_name &&
								!edit_last_name &&
								!edit_nickname &&
								!edit_phone
							}
							type="submit"
						>
							&#10004;
						</button>
						<button
							className="del-but"
							onClick={() => this.setState({ editingUnit: null })}
						>
							X
						</button>
					</li>
				</form>
			)
		}
	}

	inputHandler = (name, value) => {
		this.setState({ [name]: value })
	}

	render() {
		const { units, first_name, middle_name, last_name, nickname, phone } =
			this.state
		return (
			<div className="wrapper-ul">
				<ul>
					<li>
						<input
							onChange={(e) =>
								this.inputHandler('last_name', e.target.value)
							}
							value={last_name}
							placeholder="Фамилия работника"
						/>
						<input
							onChange={(e) =>
								this.inputHandler('first_name', e.target.value)
							}
							value={first_name}
							placeholder="Имя работника"
						/>
						<input
							onChange={(e) =>
								this.inputHandler('middle_name', e.target.value)
							}
							value={middle_name}
							placeholder="Отчество работника"
						/>
						<input
							onChange={(e) =>
								this.inputHandler('nickname', e.target.value)
							}
							value={nickname}
							placeholder="Прозвище"
						/>
						<input
							onChange={(e) =>
								this.inputHandler('phone', e.target.value)
							}
							value={phone}
							placeholder="Телефон"
						/>
						<button
							className="add-but"
							disabled={!first_name && !middle_name && !last_name}
							onClick={() => this.addUnit()}
						>
							+
						</button>
					</li>
					{units.map(this.renderUnit)}
				</ul>
			</div>
		)
	}
}

export { Units }
