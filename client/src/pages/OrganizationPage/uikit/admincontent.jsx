import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { jdeerpng } from 'src/img'

//TODO convert to a pure function
class AdminContent extends Component {
	constructor(props) {
		super(props)

		this.state = {
			name: '',
			priceforhour: '',
			priceforshift: '',
			parameters: [],
		}
	}
	componentDidMount() {
		if (this.props.tech)
			this.setState({
				name: this.props.tech.name,
				priceforhour: this.props.tech.priceforhour,
				priceforshift: this.props.tech.priceforshift,
				parameters: this.props.tech.parameter,
			})
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps.tech.name !== this.props.tech.name) {
			this.setState({ name: this.props.tech.name })
		}
		if (prevProps.tech.parameter !== this.props.tech.parameter) {
			this.setState({ parameters: this.props.tech.parameter })
		}
		if (prevProps.tech.priceforhour !== this.props.tech.priceforhour) {
			this.setState({ priceforhour: this.props.tech.priceforhour })
		}
		if (prevProps.tech.priceforshift !== this.props.tech.priceforshift) {
			this.setState({ priceforshift: this.props.tech.priceforshift })
		}
	}

	render() {
		return (
			<div className="block-techs-adm">
				<div className="block-techs-wrp-adm">
					<div className="techs-img-adm">
						<img src={jdeerpng} />
					</div>
					<div className="block-techs-descr">
						<div className="title-blc-techs">{this.state.name}</div>
						<ul>
							{this.state.parameters &&
								this.state.parameters.map((parameter, key) => {
									return (
										<li key={key}>
											{parameter.name}
											<span>{parameter.value}</span>
										</li>
									)
								})}
						</ul>
					</div>
					<div className="block-techs-right">
						<div className="block-techs-price">
							<span>Час от {this.state.priceforhour} Р</span> <br />
							<br />
							<span>Смена от {this.state.priceforshift} Р</span>
						</div>
						<div className="but-edit-techs">
							{/* <button className="add-but">+</button> */}
							<button className="edit-but">✎</button>
							<button className="del-but">X</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export { AdminContent }
