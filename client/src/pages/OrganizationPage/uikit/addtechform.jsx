import React, { Component, useEffect, useState } from 'react'
import { Formik, Field, Form, ErrorMessage, FieldArray } from 'formik'
import * as Yup from 'yup'
import { advtechService } from 'src/_services'

class AddTechForm extends Component {
	constructor(props) {
		super(props)

		this.state = {
			techTypes: [
				{
					name: 'Погрузчики',
					value: 'wheelloaders',
				},
				{
					name: 'Экскаваторы',
					value: 'excavators',
				},
				{
					name: 'Бульдозеры',
					value: 'bulldozers',
				},
				{
					name: 'Поливочные машины',
					value: 'wateringmachines',
				},
				{
					name: 'Воровайки',
					value: 'manipulators',
				},
				{
					name: 'Самосвалы',
					value: 'tippers',
				},
			],
			parameters: [],
		}
	}

	render() {
		return (
			<div className="admin-panel">
				<div className="admin-panel-wrp ">
					<Formik
						initialValues={{
							techType: 'wheelloaders',
							name: '',
							parameters: [],
							priceforhour: '',
							priceforshift: '',
							photo: null,
						}}
						validationSchema={Yup.object().shape({
							name: Yup.string().required('Введите название техники'),
							priceforhour: Yup.string().required('Введите цену'),
							priceforshift: Yup.string().required('Введите цену'),
						})}
						onSubmit={(
							{ techType, name, parameters, priceforhour, priceforshift, photo },
							{ setStatus, setSubmitting }
						) => {
							setStatus()
							advtechService
								.add({
									type: techType,
									name,
									parameter: parameters,
									priceforhour,
									priceforshift,
									photo,
								})
								.then(
									() => {
										const { from } = this.props.location.state || {
											from: { pathname: '/grafik' },
										}
										this.props.history.push(from)
									},
									(error) => {
										setSubmitting(false)
										setStatus(error)
									}
								)
						}}
					>
						{({
							errors,
							// status,
							touched,
							isSubmitting,
							values,
							setFieldValue,
						}) => (
							<Form>
								<div className="form-group">
									<label htmlFor="techType">Тип техники</label>
									<Field component="select" name="techType">
										{this.state.techTypes.map((item, key) => {
											return (
												<option key={key} value={item.value}>
													{item.name}
												</option>
											)
										})}
									</Field>
									{/* <Field name="techType" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} /> */}
									<ErrorMessage name="techType" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="name">Название техники</label>
									<Field
										name="name"
										type="text"
										className={
											'form-control' + (errors.username && touched.username ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="name" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="priceforhour">Цена за час</label>
									<Field
										name="priceforhour"
										type="text"
										className={
											'form-control' + (errors.username && touched.username ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="priceforhour" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="priceforshift">Цена за смену</label>
									<Field
										name="priceforshift"
										type="text"
										className={
											'form-control' + (errors.username && touched.username ? ' is-invalid' : '')
										}
									/>
									<ErrorMessage name="priceforshift" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="parameters">Параметры</label>
									<FieldArray
										name="parameters"
										className={
											'form-control' + (errors.username && touched.username ? ' is-invalid' : '')
										}
									>
										{(arrayHelpers) => {
											return (
												<div>
													{values.parameters && values.parameters.length > 0 ? (
														values.parameters.map((item, index) => (
															<div key={index}>
																<Field name={`parameters.${index}.name`} type="text" />
																<Field name={`parameters.${index}.value`} type="text" />
																<button
																	className="add-parameter"
																	type="button"
																	onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
																>
																	-
																</button>
																<button
																	className="add-parameter next"
																	type="button"
																	onClick={() => arrayHelpers.push()} // insert an empty string at a position
																>
																	+
																</button>
															</div>
														))
													) : (
														<button type="button" onClick={() => arrayHelpers.push('')}>
															{/* show this when user has removed all friends from the list */}
															Добавить параметр
														</button>
													)}
												</div>
											)
										}}
									</FieldArray>
									<ErrorMessage name="parameters" component="div" className="invalid-feedback" />
								</div>
								<div className="form-group">
									<label htmlFor="photo">Загрузить изображение</label>
									<input
										name="photo"
										type="file"
										onChange={(event) => {
											setFieldValue('photo', event.currentTarget.files[0])
										}}
										className="form-control"
									/>
									<Thumb file={values.photo} />
								</div>
								{!isSubmitting && (
									<button type="submit" className="btn btn-primary" disabled={isSubmitting}>
										Добавить
									</button>
								)}
							</Form>
						)}
					</Formik>
				</div>
			</div>
		)
	}
}

const Thumb = ({ file }) => {
	const [loading, setLoading] = useState(false)
	const [thumb, setThumb] = useState(undefined)

	useEffect(() => {
		setLoading(true)
		let reader = new FileReader()
		reader.onloadend = () => {
			this.setState({ loading: false, thumb: reader.result })
		}
		reader.readAsDataURL(file)
	}, [file])

	if (!file) {
		return null
	}

	if (loading) {
		return <p>loading...</p>
	}

	return (
		<img
			src={thumb}
			alt={file.name}
			className="img-thumbnail mt-2"
			height={200}
			width={200}
		/>
	)
}

export { AddTechForm }
