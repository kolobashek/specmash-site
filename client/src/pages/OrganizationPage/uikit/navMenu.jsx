import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import { useAuth } from 'src/_services'
import 'pages/OrganizationPage/TablePage/components/Grafik-styles.scss'

const NavMenu = () => {
	const auth = useAuth()
	const { pathname } = useLocation()
	const { isAdmin, isUser } = auth
	return auth.currentUser ? (
		!isAdmin && !isUser ? (
			<div className="navMenuContainer">
				<a onClick={() => auth.logout()}>Выход</a>
			</div>
		) : (
			<div className="navMenuContainer">
				{isAdmin && (
					<React.Fragment>
						{/* <Link to={`/org/adminsait`}>Админ сайта</Link> */}
						<Link to={`/org`}>График</Link>
						<Link to={`/org/techs`}>Техника</Link>
						<Link to={`/org/objects`}>Объекты</Link>
						<Link to={`/org/units`}>Работники</Link>
						<Link to={`/org/user`}>Личный кабинет</Link>
						{/* <Link to={`org/admin`}>
								Админка
							</Link> */}
						<Link to={''} onClick={() => auth.logout()}>
							Выход
						</Link>
					</React.Fragment>
				)}
				{isUser && (
					<React.Fragment>
						<Link to={`/org`}>График</Link>
						<Link to={''} onClick={() => auth.logout()}>
							Выход
						</Link>
					</React.Fragment>
				)}
			</div>
		)
	) : (
		<div className="navMenuContainer">
			<Link to={`/login`}>Войти</Link>
		</div>
	)
}

export { NavMenu }
