import React from 'react'
// import { Footer } from './uikit'
import { Link } from 'react-router-dom'
import {
	minPogruzchik,
	minSHACMAN,
	minManipulyator,
	minHyunday250png,
	minPolivochnyeMashiny,
	shantui32,
} from 'img'

const Arenda = () => {
	return (
		<React.Fragment>
			<div className="arenda-container">
				<div className="catalog-container">
					<div className="arenda-deskr">
						<span className="arenda-deskr-wrp">
							{`Компания ООО "Спецмаш" предоставляет услуги аренды
							спецтехники в Прокопьевске и Прокопьевском районе.
							Более десяти лет мы работаем на рынке данных услуг.
							Наш автопарк состоит из современной, качественной и
							надежной российской и импортной техники различного
							назначения, включая: Бульдозеры - Автокраны –
							Фронтальные погрузчики– Экскаваторы – Манипуляторы –
							Самосвалы – Поливочные машины. Спецтехника нашей
							компании отличается высокой надежностью и
							функциональностью.`}
						</span>
					</div>
					<div className="catalog">
						<div className="catalog-item catalog-porgruzchiki">
							<img src={minPogruzchik} />
							<div className="cat-h3">
								<h3>Фронтальные погрузчики</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link
									to="/wheelloaders"
									className="but-catalog"
								>
									Подробнее
								</Link>
							</div>
						</div>
						<div className="catalog-item catalog-samosvaly">
							<img src={minSHACMAN} />
							<div className="cat-h3">
								<h3>Самосвалы</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link to="/tippers" className="but-catalog">
									Подробнее
								</Link>
							</div>
						</div>
						<div className="catalog-item catalog-manipulyatory">
							<img src={minManipulyator} />
							<div className="cat-h3">
								<h3>Краны манипуляторы</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link
									to="/manipulators"
									className="but-catalog"
								>
									Подробнее
								</Link>
							</div>
						</div>
						<div className="catalog-item catalog-excovatory">
							<img src={minHyunday250png} />
							<div className="cat-h3">
								<h3>Эксковаторы</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link to="/excavators" className="but-catalog">
									Подробнее
								</Link>
							</div>
						</div>
						<div className="catalog-item catalog-polivochnye">
							<img src={minPolivochnyeMashiny} />
							<div className="cat-h3">
								<h3>Поливочные машины</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link
									to="/wateringmachines"
									className="but-catalog"
								>
									Подробнее
								</Link>
							</div>
						</div>
						<div className="catalog-item catalog-buldozery">
							<img src={shantui32} />
							<div className="cat-h3">
								<h3>Бульдозеры</h3>
							</div>
							<div className="but-catalog-wrp">
								<Link to="/bulldozers" className="but-catalog">
									Подробнее
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* <Footer /> */}
		</React.Fragment>
	)
}

export { Arenda }
