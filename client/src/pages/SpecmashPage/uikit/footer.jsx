import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<div className="footer-wrp">
					<div className="menu-footer">
						<div className="column-menu-foot">
							<ul>
								<li>
									<Link className="footer-a" to="/">
										Главная
									</Link>
								</li>
								<li>
									<Link className="footer-a" to="/">
										Услуги
									</Link>
								</li>
								<li>
									<Link className="footer-a" to="/contacts">
										Контакты
									</Link>
								</li>
								<li>
									<Link className="footer-a" to="/">
										Вакансии
									</Link>
								</li>
							</ul>
						</div>
						<div className="column-menu-foot">
							<ul>
								<li>
									<Link
										className="footer-a"
										to="/wheelloaders"
									>
										Фронтальные погрузчики
									</Link>
								</li>
								<li>
									<Link className="footer-a" to="/tippers">
										Самосвалы
									</Link>
								</li>
								<li>
									<Link
										className="footer-a"
										to="/manipulators"
									>
										Краны манипуляторы
									</Link>
								</li>
							</ul>
						</div>
						<div className="column-menu-foot">
							<ul>
								<li>
									<Link className="footer-a" to="/excavators">
										Экскаваторы
									</Link>
								</li>
								<li>
									<Link
										className="footer-a"
										to="/wateringmachines"
									>
										Поливочные машины
									</Link>
								</li>
								<li>
									<Link className="footer-a" to="/bulldozers">
										Бульдозеры
									</Link>
								</li>
							</ul>
						</div>
					</div>
					<div className="footer-right">
						<div className="footer-phone">
							<a href="tel:89521701660">8 (952) 170-16-60</a>
							<br />
							<a href="tel:89134053989">8 (913) 405-39-89</a>
						</div>
						<div className="footer-email">specmash.prk@mail.ru</div>
					</div>
				</div>
				<a href="https://web-azimuth.ru/">
					<span>Разработано Веб-студией AZIMUTH</span>
				</a>
			</div>
		)
	}
}

export { Footer }
