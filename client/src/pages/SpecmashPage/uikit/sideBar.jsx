import React, { useEffect, useState } from 'react'
import Sidebar from 'react-sidebar'
import { Link } from 'react-router-dom'
import { logo, phonegif, contactsIcon, glavnayaIcon, graficIcon, workIcon } from 'img'
import { FaTractor } from 'react-icons/fa'
import { ImEnter } from 'react-icons/im'
import { MdMenu } from 'react-icons/md'

const mql = window.matchMedia(`(min-width: 800px)`)

const SideBar = ({ children }) => {
	const [sidebarOpen, setSidebarOpen] = useState(false)
	const [sidebarDocked, setSidebarDocked] = useState(mql.matches)
	// this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this)

	useEffect(() => {
		mql.addListener(mediaQueryChanged)
	}, [])

	const onSetSidebarOpen = (open) => {
		setSidebarOpen(open)
	}

	const mediaQueryChanged = () => {
		setSidebarDocked(mql.matches)
		setSidebarOpen(false)
	}
	const menu = (
		<div className="sidebar-menu">
			<div className="logo-content">
				<div className="logo-wrp">
					<div className="logo">
						<Link to="/">
							<img src={logo} alt="" />
						</Link>
					</div>
				</div>
			</div>
			<ul>
				<li>
					<div className="icon-sidebar">
						<img src={glavnayaIcon} alt="" />
					</div>
					<div>
						<Link to="/">Главная</Link>
					</div>
				</li>
				{/* <li><Link to='/'>Услуги</Link></li> */}
				<li>
					<div className="icon-sidebar">
						<FaTractor size={30} className="sidebar-icon" />
					</div>
					<div>
						<Link to="/arenda" style={{ textDecoration: 'none' }}>
							Аренда
						</Link>
					</div>
				</li>
				<li>
					<div className="icon-sidebar">
						<img src={contactsIcon} alt="" />
					</div>
					<div>
						<Link to="/contacts">Контакты</Link>
					</div>
				</li>
				<li>
					<div className="icon-sidebar">
						<img src={workIcon} alt="" />
					</div>
					<div>
						<Link to="/">Вакансии</Link>
					</div>
				</li>
				<li>
					<div className="icon-sidebar">
						<img src={graficIcon} alt="" />
					</div>
					<div>
						<Link to="/grafik">График</Link>
					</div>
				</li>
				<li>
					<div className="icon-sidebar">
						<ImEnter size={25} className="sidebar-icon" />
					</div>
					<div>
						<Link to="/login" style={{ textDecoration: 'none' }}>
							Вход
						</Link>
					</div>
				</li>
				<li>
					<div className="icon-sidebar">
						<img src={phonegif} alt="" />
					</div>
					<div>
						<a className="header-phone" href="tel:8 (951) 601-72-95">
							Позвонить
						</a>
					</div>
				</li>
			</ul>
		</div>
	)
	return (
		<Sidebar
			sidebar={menu}
			open={sidebarOpen}
			onSetOpen={onSetSidebarOpen}
			// docked={sidebarDocked}
			styles={{ sidebar: { background: 'white' } }}
		>
			<div className="headerMobile">
				<a onClick={() => onSetSidebarOpen(true)} className="mobile-menu">
					<MdMenu size={64} className="MobileMenu-icon" />
				</a>
				<div>
					<h1>{`ООО "Спецмаш"`}</h1>
				</div>
			</div>

			<div className="sideBarContentWrapper">{children}</div>
		</Sidebar>
	)
}

export { SideBar }
