import React, { Component } from 'react'

class Maincontent extends Component {
	render() {
		return (
			<div>
				<div className="item-content">
					<div className="culomn pogruz-img"></div>
					<div className="culomn cul-pdg">
						<div className="cul-title">
							<h2>Услуги погрузчика</h2>
						</div>
						<div className="cul-descr">
							Погрузка различных материалов
							<br />
							карьерные, земельные работы
							<br />
							чистка снега
							<br />
						</div>
						<div>
							<button className="cul-but">Заказать</button>
						</div>
					</div>
				</div>

				<div className="item-content">
					<div className="culomn cul-pdg">
						<div className="cul-title">
							<h2>Услуги манипулятора</h2>
						</div>
						<div className="cul-descr">
							Погрузка-транспортировка грузов
						</div>
						<div>
							<button className="cul-but">Заказать</button>
						</div>
					</div>
					<div className="culomn monipul-img"></div>
				</div>

				<div className="item-content">
					<div className="culomn samosval-img"></div>
					<div className="culomn cul-pdg">
						<div className="cul-title">
							<h2>Услуги самосвалов</h2>
						</div>
						<div className="cul-descr">
							Перевозка сыпучих грузов
						</div>
						<div>
							<button className="cul-but">Заказать</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export { Maincontent }
