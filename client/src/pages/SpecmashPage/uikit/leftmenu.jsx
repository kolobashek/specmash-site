import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Leftmenu extends Component {
	render() {
		return (
			<div>
				<div className="left-menu">
					<ul>
						<li>
							<Link to="/wheelloaders">
								Фронтальные погрузчики
							</Link>
						</li>
						<li>
							<Link to="/tippers">Самосвалы</Link>
						</li>
						<li>
							<Link to="/manipulators">Краны манипуляторы</Link>
						</li>
						<li>
							<Link to="/excavators">Экскаваторы</Link>
						</li>
						<li>
							<Link to="/wateringmachines">
								Поливочные машины
							</Link>
						</li>
						<li>
							<Link to="/bulldozers">Бульдозеры</Link>
						</li>
					</ul>
				</div>
			</div>
		)
	}
}

export { Leftmenu }
