import React from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from 'src/_services'
// import { history } from 'src/_helpers'
import { logo } from 'img'
import { ImEnter } from 'react-icons/im'
import { ImExit } from 'react-icons/im'

const Header = () => {
	const auth = useAuth()

	const { currentUser, isAdmin, isUser } = auth
	return (
		<div className="container-header-wrp">
			<div className="container-header">
				<div className="logo-content">
					<div className="logo-wrp">
						<div className="logo">
							<Link to="/">
								<img src={logo} alt="" />
							</Link>
						</div>
					</div>
				</div>
				<div className="top-menu">
					<ul>
						<li>
							<Link to="/">Главная</Link>
						</li>
						{/* <li><Link to='/'>Услуги</Link></li> */}
						<li>
							<Link to="/arenda" style={{ textDecoration: 'none' }}>
								Аренда
							</Link>
						</li>
						<li>
							<Link to="/contacts">Контакты</Link>
						</li>
						<li>
							<Link to="/">Вакансии</Link>
						</li>
						{(isAdmin || isUser) && (
							<li>
								<Link to="/org">График</Link>
							</li>
						)}
					</ul>
				</div>
				<div className="header-phone-cont">
					<div className="header-phone-wrp">
						<a className="header-phone" href="tel:89521701660">
							8 (952) 170-16-60
						</a>
						<br />
						<a className="header-phone" href="tel:89134053989">
							8 (913) 405-39-89
						</a>
					</div>
					{currentUser ? (
						<a onClick={() => auth.logout()} className="butt-login">
							<ImExit size={20} className="exit-icon" />
							Выход
						</a>
					) : (
						<Link to="/login" className="butt-login">
							<ImEnter size={20} className="enter-icon" />
							Вход
						</Link>
					)}
				</div>
			</div>
		</div>
	)
}

export { Header }
