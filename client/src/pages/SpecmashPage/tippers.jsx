import React, { Component } from 'react'
import { Leftmenu, TechBlock } from './uikit'
// import { Link } from 'react-router-dom'
import { jdeerpng } from 'img'
import { advtechService } from 'src/_services'

class Tippers extends Component {
	constructor(props) {
		super(props)

		this.state = {
			advTechs: [],
		}
	}
	async componentDidMount() {
		advtechService
			.getAll('tippers')
			.then((advTechs) => this.setState({ advTechs }))
	}
	render() {
		return (
			<div>
				<div className="content-techs">
					<div className="cont-techs-wrp ">
						<div className="block-techs">
							<Leftmenu />
						</div>
						{this.state.advTechs.map((item, key) => {
							return <TechBlock key={key} tech={item} />
						})}
					</div>
				</div>
			</div>
		)
	}
}

export { Tippers }
