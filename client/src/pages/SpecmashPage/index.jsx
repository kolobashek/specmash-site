import React from 'react'
// import { PrivateRoute } from 'src/_components'
import { Wheelloaders, Tippers, Manipulators, Excavators, Wateringmachines, Bulldozers } from 'src/pages/SpecmashPage'
import { Header, Footer } from 'src/pages/SpecmashPage/uikit'
import 'pages/style.scss'
import 'pages/optimization.scss'
import { Route, Switch } from 'react-router-dom'

const MainPage = () => {
	// let { path, url } = useRouteMatch()
	return (
		<>
			<Header />
			<Switch>
				<Route path="/wheelloaders">
					<Wheelloaders />
				</Route>
				<Route path="/tippers">
					<Tippers />
				</Route>
				<Route path="/manipulators">
					<Manipulators />
				</Route>
				<Route path="/excavators">
					<Excavators />
				</Route>
				<Route path="/wateringmachines">
					<Wateringmachines />
				</Route>
				<Route path="/bulldozers">
					<Bulldozers />
				</Route>
			</Switch>
			<Footer />
			{/* <PrivateRoute exact path="/" component={HomePage} /> */}
			{/* <PrivateRoute path="/admin" roles={[Role.Admin]} component={AdminPage} /> */}
			{/* </div>
                            </div>
                        </div>
                    </div> */}
		</>
	)
}

export { MainPage }

export * from './tippers'
export * from './wheelloaders'
export * from './manipulators'
export * from './excavators'
export * from './wateringmachines'
export * from './bulldozers'
export * from '../arenda'
