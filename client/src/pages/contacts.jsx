import React from 'react'
import { YMaps, Map, Placemark } from 'react-yandex-maps'

const Contacts = () => {
	const placeMark = {
		geometry: [53.88514, 86.727526],
		properties: {
			hintContent: 'Спецмаш',
			balloonContent: 'Спецмаш МАГАЗИН',
		},
		modules: ['geoObject.addon.balloon', 'geoObject.addon.hint'],
	}
	return (
		<div>
			<div className="content-contacts">
				<div className="map-container">
					<YMaps>
						<div>
							<Map
								defaultState={{
									center: [53.88514, 86.727526],
									zoom: 16,
								}}
								height="400px"
								width="1280"
							>
								<Placemark {...placeMark} />
							</Map>
						</div>
					</YMaps>
					{/* <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad06ca3a87cc33197c75dbedc53c71928ded53bda28e03127e94ffaa6d82eeef1&amp;width=1280&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script> */}
				</div>
				<div className="contacts-data">
					<div className="contacts-data-wrp">
						<div className="contacts-item">
							<a href="tel:89521701660">8 (952) 170-16-60</a>
						</div>
						<div className="contacts-item">
							<a href="tel:89134053989">8 (913) 405-39-89</a>
						</div>
						<div className="contacts-item">
							specmash.prk@mail.ru
						</div>
						<div className="contacts-item">
							г. Прокопьевск ул. Розы люксембург, 5
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export { Contacts }
