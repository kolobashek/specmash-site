import React from 'react'
import { Route, Redirect } from 'react-router-dom'

import { useAuth } from 'src/_services'

export const PrivateRoute = ({ children, roles, ...rest }) => {
	const { user } = useAuth()
	return (
		<Route
			{...rest}
			render={(props) => {
				if (!user) {
					// not logged in so redirect to login page with the return url
					return (
						<Redirect
							to={{
								pathname: '/login',
								state: { from: props.location },
							}}
						/>
					)
				}

				// check if route is restricted by role
				if (roles && roles.indexOf(user.role) === -1) {
					// role not authorised so redirect to home page
					return <Redirect to={{ pathname: '/' }} />
				}

				// authorised so return component
				return children
			}}
		/>
	)
}
