import { authHeader, handleResponse } from 'src/_helpers'

export const advtechService = {
	getAll,
	add,
	del,
	update,
}

function getAll(data) {
	const requestOptions = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/advtechs`, requestOptions).then(
		handleResponse
	)
}

function add(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/advtechs/add`, requestOptions).then(
		handleResponse
	)
}

function del(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/advtechs/delete`, requestOptions)
		.then(handleResponse)
		.then((shift) => {
			return shift
		})
}
function update(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/advtechs/update`, requestOptions)
		.then(handleResponse)
		.then((shift) => {
			return shift
		})
}
