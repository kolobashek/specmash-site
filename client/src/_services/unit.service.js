import { authHeader, handleResponse } from 'src/_helpers'

export const unitService = {
	getAll,
	add,
	del,
	update,
}

function getAll() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/units`, requestOptions).then(handleResponse)
}

function add(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/units/add`, requestOptions).then(handleResponse)
}

function del(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/units/delete`, requestOptions).then(handleResponse)
}
function update(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/units/update`, requestOptions).then(handleResponse)
}
