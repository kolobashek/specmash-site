import React, { useState, useEffect, useContext, createContext } from 'react'
import { handleResponse } from 'src/_helpers'

const authContext = createContext()

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export const ProvideAuth = ({ children }) => {
	const auth = useProvideAuth()
	return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = () => {
	return useContext(authContext)
}

// Provider hook that creates auth object and handles state
function useProvideAuth() {
	const [user, setUser] = useState(null)
	const login = (username, password) => {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ username, password }),
		}
		return fetch(`${process.env.API_URL}/users/authenticate`, requestOptions)
			.then(handleResponse)
			.then((reqUser) => {
				// store user details and jwt token in local storage to keep user logged in between page refreshes
				localStorage.setItem('currentUser', JSON.stringify(reqUser))
				setUser(reqUser)
				return reqUser
			})
	}
	const register = ({ username, password, firstname, lastname, email, phone }) => {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				username,
				password,
				firstname,
				lastname,
				email,
				phone,
			}),
		}
		return fetch(`${process.env.API_URL}/users/register`, requestOptions)
			.then(handleResponse)
			.then((reqUser) => {
				// store user details and jwt token in local storage to keep user logged in between page refreshes
				localStorage.setItem('currentUser', JSON.stringify(reqUser))
				setUser(reqUser)
				return reqUser
			})
	}
	const logout = () => {
		// remove user from local storage to log user out
		localStorage.removeItem('currentUser')
		location.reload(true)
		setUser(false)
	}
	const isAdmin = user && user.role === 'Admin' ? true : false
	const isUser = user && user.role === 'User' ? true : false
	// Subscribe to user on mount
	// Because this sets state in the callback it will cause any ...
	// ... component that utilizes this hook to re-render with the ...
	// ... latest auth object.
	useEffect(() => {
		const localStorageUser = JSON.parse(localStorage.getItem('currentUser'))
		if (localStorageUser) {
			setUser(localStorageUser)
		} else {
			setUser(false)
		}
		// const unsubscribe = () => {
		// 	const localStorageUser = JSON.parse(
		// 		localStorage.getItem('currentUser')
		// 	)
		// 	if (localStorageUser) {
		// 		setUser(localStorageUser)
		// 	} else {
		// 		setUser(false)
		// 	}
		// }
		// // Cleanup subscription on unmount
		// return () => unsubscribe()
	}, [])
	// Return the user object and auth methods
	return {
		user,
		currentUser: user,
		login,
		logout,
		register,
		isAdmin,
		isUser,
		// sendPasswordResetEmail,
		// confirmPasswordReset,
	}
}
