import { authHeader, handleResponse } from 'src/_helpers'

export const objectService = {
	getAll,
	getAllActive,
	add,
	del,
	update,
}

function getAll() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/objects`, requestOptions).then(
		handleResponse
	)
}

function getAllActive() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/objects/active`, requestOptions).then(
		handleResponse
	)
}

function add(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/objects/add`, requestOptions)
		.then(handleResponse)
		.then((newobject) => {
			return newobject
		})
}

function del(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/objects/delete`, requestOptions)
		.then(handleResponse)
		.then((tech) => {
			return tech
		})
}
function update(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/objects/update`, requestOptions)
		.then(handleResponse)
		.then((tech) => {
			return tech
		})
}
