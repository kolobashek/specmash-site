import { authHeader, handleResponse } from 'src/_helpers'

export const techService = {
	getAll,
	getAllActive,
	add,
	del,
	update,
}

function getAll() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/techs`, requestOptions).then(
		handleResponse
	)
}

function getAllActive() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/techs/active`, requestOptions).then(handleResponse)
}

function add(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/techs/add`, requestOptions)
		.then(handleResponse)
		.then((newtech) => {
			return newtech
		})
}

function del(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/techs/delete`, requestOptions)
		.then(handleResponse)
		.then((tech) => {
			return tech
		})
}
function update(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/techs/update`, requestOptions)
		.then(handleResponse)
		.then((tech) => {
			return tech
		})
}
