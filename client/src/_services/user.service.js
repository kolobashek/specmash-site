import { authHeader, handleResponse } from 'src/_helpers'

export const userService = {
	getAll,
	getById,
	update,
}

function getAll() {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/users`, requestOptions).then(handleResponse)
}

function getById(id) {
	const requestOptions = { method: 'GET', headers: authHeader() }
	return fetch(`${process.env.API_URL}/users/${id}`, requestOptions).then(handleResponse)
}

function update(data) {
	const requestOptions = {
		method: 'POST',
		headers: authHeader('post'),
		body: JSON.stringify({ data }),
	}
	return fetch(`${process.env.API_URL}/users/update`, requestOptions).then(handleResponse)
}
