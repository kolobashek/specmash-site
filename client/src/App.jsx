import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ProvideAuth } from 'src/_services'
import { Organization } from 'src/pages'
import 'pages/style.scss'
import 'pages/optimization.scss'
import { PrivateRoute } from 'src/_components'
import { Catalog } from './pages/catalog'
import { LoginPage } from './pages/LoginPage'
import { Contacts } from './pages/contacts'
import { Arenda } from './pages/arenda'
import { Header } from './pages/SpecmashPage/uikit/header'
import { Footer } from './pages/SpecmashPage/uikit/footer'
import { SideBar } from './pages/SpecmashPage/uikit'

const App = () => {
	return (
		<ProvideAuth>
			<Router>
				<SideBar>
					<Switch>
						<Route path="/" exact>
							<Header />
							<Catalog />
							<Footer />
						</Route>
						<Route path="/login">
							<Header />
							<LoginPage />
							<Footer />
						</Route>
						<Route path="/contacts">
							<Header />
							<Contacts />
							<Footer />
						</Route>
						<Route path="/arenda">
							<Header />
							<Arenda />
							<Footer />
						</Route>
						<PrivateRoute path="/org">
							<Organization />
						</PrivateRoute>
					</Switch>
				</SideBar>
			</Router>
		</ProvideAuth>
	)
}

export { App }
