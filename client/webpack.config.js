var HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const Dotenv = require('dotenv-webpack')

module.exports = {
	mode: 'development',
	// mode: 'production',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				include: [path.resolve(__dirname, 'src')],
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env'],
				},
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						// options: {
						// sourceMap: true,
						// sassOptions: {
						//     outputStyle: "compressed",
						// },
						// },
					},
				],
			},
			{
				test: /\.(png|svg|jpe?g|gif|ico)$/i,
				use: [
					{
						loader: 'file-loader',
					},
				],
			},
			{
				test: /\.js$/,
				enforce: 'pre',
				use: ['source-map-loader'],
			},
		],
	},
	resolve: {
		extensions: ['.js', '.jsx', '.png', '.jpeg', '.scss', '.svg'],
		modules: [path.resolve(__dirname + '/node_modules')],
		alias: {
			src: path.resolve(__dirname + '/src'),
			img: path.resolve(__dirname + '/src/img'),
			pages: path.resolve(__dirname + '/src/pages'),
		},
		// roots: [
		//     path.resolve(__dirname  + '/src')
		// ]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
			favicon: './src/img/favicon.ico',
		}),
		new Dotenv(),
	],
	devServer: {
		historyApiFallback: true,
	},
	devtool: 'source-map',
	// externals: {
	//     // global app config object
	//     config: JSON.stringify({
	//         apiUrl: 'http://specmash.su:4000'
	//         // apiUrl: 'http://localhost:4000'
	//     })
	// },
	output: {
		publicPath: '/',
	},
	target: 'node',
}