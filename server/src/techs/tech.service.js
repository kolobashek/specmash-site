﻿const models = require('../_helpers/models')

const techs = models.tech

module.exports = {
	getAll,
	getAllActive,
	getById,
	updateById,
	del,
	add,
}

async function add(body) {
	return techs.create(body).then((newtech) => {
		return newtech
	})
}

async function del(body) {
	let { id, status } = body.data
	status = !status
	return techs
		.findByPk(id)
		.then((Tech) => {
			if (Tech) {
				console.log('resStat>>>', Tech.status, status)
				return Tech.update({ status })
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function updateById(body) {
	const { id, ...values } = body.data
	return techs
		.findByPk(id)
		.then((Tech) => {
			if (Tech) {
				return Tech.update(values)
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function getAllActive() {
	return techs.findAll({
		where: {
			status: 1,
		},
	})
}

async function getAll() {
	return techs.findAll()
}

async function getById(id) {
	const tech = techs.findOne({ where: { id: id } }).then((cb) => {
		const Tech = cb.get()
		if (!Tech) return
		return Tech
	})
	return tech
}
