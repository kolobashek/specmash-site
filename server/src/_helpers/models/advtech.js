module.exports = function (sequelize, Sequelize) {
	var AdvTech = sequelize.define('advtech', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		type: {
			type: Sequelize.ENUM(
				'bulldozers',
				'excavators',
				'manipulators',
				'tippers',
				'wateringmachines',
				'wheelloaders'
			),
		},
		photo: { type: Sequelize.BLOB('long') },
		name: { type: Sequelize.STRING },
		parameter: {
			type: Sequelize.STRING,
			get: function () {
				return JSON.parse(this.getDataValue('parameter'))
			},
			set: function (val) {
				return this.setDataValue('parameter', JSON.stringify(val))
			},
		},
		priceforhour: { type: Sequelize.INTEGER },
		priceforshift: { type: Sequelize.INTEGER },
		status: { type: Sequelize.BOOLEAN, defaultValue: true },
	})

	return AdvTech
}
