module.exports = function (sequelize, Sequelize) {
	var Unit = sequelize.define('unit', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		first_name: { type: Sequelize.STRING },
		middle_name: { type: Sequelize.STRING },
		last_name: { type: Sequelize.STRING },
		nickname: { type: Sequelize.STRING },
		phone: { type: Sequelize.STRING },
		about: { type: Sequelize.TEXT },
		status: { type: Sequelize.BOOLEAN, defaultValue: true },
	})

	return Unit
}
