module.exports = function (sequelize, Sequelize) {
	var User = sequelize.define('user', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		firstname: { type: Sequelize.STRING, notEmpty: true },
		lastname: { type: Sequelize.STRING, notEmpty: true },
		username: { type: Sequelize.TEXT },
		password: { type: Sequelize.STRING, allowNull: false },
		role: { type: Sequelize.TEXT },
		about: { type: Sequelize.TEXT },
		email: { type: Sequelize.STRING },
		phone: { type: Sequelize.STRING, notEmpty: true },
		last_login: { type: Sequelize.DATE },
		status: { type: Sequelize.BOOLEAN, defaultValue: true },
	})

	return User
}
