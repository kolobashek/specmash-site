module.exports = function (sequelize, Sequelize) {
	var Object = sequelize.define('object', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		name: { type: Sequelize.STRING, notEmpty: true },
		comment: { type: Sequelize.STRING },
		status: { type: Sequelize.BOOLEAN, defaultValue: true },
	})

	return Object
}
