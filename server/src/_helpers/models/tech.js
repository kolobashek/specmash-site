module.exports = function (sequelize, Sequelize) {
	var Tech = sequelize.define('tech', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		model: { type: Sequelize.STRING },
		bort: { type: Sequelize.INTEGER, notEmpty: true },
		govNumber: { type: Sequelize.STRING },
		nickname: { type: Sequelize.STRING },
		about: { type: Sequelize.TEXT },
		status: { type: Sequelize.BOOLEAN, defaultValue: true },
	})

	return Tech
}
