module.exports = function (sequelize, Sequelize) {
	var Shift = sequelize.define('shift', {
		id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
		date: { type: Sequelize.INTEGER },
		half: { type: Sequelize.ENUM('1', '2') },
		unit: { type: Sequelize.INTEGER },
		tech: { type: Sequelize.INTEGER },
		object: { type: Sequelize.INTEGER },
		comment: { type: Sequelize.TEXT },
	})

	return Shift
}
