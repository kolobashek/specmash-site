﻿const config = require('config.json')
const jwt = require('jsonwebtoken')
const Role = require('../_helpers/role')
const models = require('../_helpers/models')
const bCrypt = require('bcrypt-nodejs')

// users hardcoded for simplicity, store in a db for production applications
// const users = [
//     { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
//     { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User }
// ];
const units = models.unit

module.exports = {
	getAll,
	getById,
	updateById,
	del,
	add,
}

async function add(body) {
	return units.create(body).then((newunit) => {
		return newunit
	})
}

async function del(body) {
	let { id, status } = body.data
	// eslint-disable-next-line no-constant-condition
	status = !status
	return units
		.findByPk(id)
		.then((Unit) => {
			if (Unit) {
				return Unit.update({ status })
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function updateById(body) {
	const { id, ...values } = body.data
	console.log(values)
	return units
		.findByPk(id)
		.then((unit) => {
			if (unit) {
				return unit
					.update(values)
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function getAll() {
	return units.findAll()
}

async function getById(id) {
	const unit = units.findOne({ where: { id: id } }).then((cb) => {
		const Unit = cb.get()
		if (!Unit) return
		return Unit
	})
	return unit
}
