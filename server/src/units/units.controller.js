const express = require('express')
const router = express.Router()
const unitService = require('./unit.service')
const authorize = require('../_helpers/authorize')
const Role = require('../_helpers/role')

// routes
router.post('/update', authorize(Role.Admin), update) // public route
router.post('/add', authorize(Role.Admin), add)
router.post('/delete', authorize(Role.Admin), del)
// router.get('/', authorize(Role.Admin), getAll); // admin only
router.get('/:id', authorize(), getById) // all authenticated users
router.get('/', getAll) // admin only
module.exports = router

function update(req, res, next) {
	unitService
		.updateById(req.body)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({ message: 'Something went wrong!' })
		})
		.catch((err) => next(err))
}

function del(req, res, next) {
	unitService
		.del(req.body)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({ message: 'Something went wrong!' })
		})
		.catch((err) => next(err))
}

function getAll(req, res, next) {
	unitService
		.getAll()
		.then((users) => res.json(users))
		.catch((err) => next(err))
}

function getById(req, res, next) {
	const currentUser = req.user
	const id = parseInt(req.params.id)

	// only allow admins to access other user records
	if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
		return res.status(401).json({ message: 'Unauthorized' })
	}

	unitService
		.getById(req.params.id)
		.then((user) => (user ? res.json(user) : res.sendStatus(404)))
		.catch((err) => next(err))
}

function add(req, res, next) {
	unitService
		.add(req.body.data)
		.then((newunit) => {
			res.json(newunit)
		})
		.catch((err) => next(err))
}
