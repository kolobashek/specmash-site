﻿require('rootpath')()
require('dotenv').config()
const express = require('express')
const cors = require('cors')
// const fs = require('fs')
// const fs = require('file-system')
// const http = require('http')
// const https = require('https')
const errorHandler = require('./_helpers/error-handler')
const models = require('./_helpers/models')
const mysql = require('mysql2')
const config = require('config.json')

// const privateKey  = fs.readFileSync('private_assets/server.key');
// const certificate = fs.readFileSync('private_assets/server.cert');
// const key  = fs.readFileSync('private_assets/privkey.pem');
// const cert = fs.readFileSync('private_assets/fullchain.pem');
// const credentials = {key, cert};

const opts = process.env.NODE_ENV || 'connection'
const connection = mysql.createConnection(config[opts])
connection.connect((err) => {
	if (err) {
		return console.error('Ошибка: ' + err.message)
	} else {
		console.log('Подключение к серверу MySQL успешно установлено')
	}
})

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())

models.sequelize
	.sync()
	.then(function () {
		console.log('Nice! Database looks fine')
		console.log(`environment: ${process.env.NODE_ENV}`)
	})
	.catch(function (err) {
		console.log(err, 'Something went wrong with the Database Update!')
	})

// api routes
app.use('/users', require('./users/users.controller'))
app.use('/techs', require('./techs/techs.controller'))
app.use('/units', require('./units/units.controller'))
app.use('/shifts', require('./shifts/shifts.controller'))
app.use('/advtechs', require('./advtech/advtechs.controller'))
app.use('/objects', require('./objects/object.controller'))

// global error handler
app.use(errorHandler)

// start server
// const httpsServer = https.createServer(credentials, app)
// const httpServer = http.createServer(app)
const port = process.env.NODE_ENV === 'production' ? 4001 : 4000
app.listen(port, function () {
	console.log('Server listening on port ' + port)
})
