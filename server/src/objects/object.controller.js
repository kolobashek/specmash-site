const express = require('express')
const router = express.Router()
const objService = require('./object.service')
const authorize = require('../_helpers/authorize')
const Role = require('../_helpers/role')

// routes
router.post('/update', authorize(Role.Admin), update) // public route
router.post('/add', authorize(Role.Admin), add)
router.post('/delete', authorize(Role.Admin), del)
router.get('/active', getAllActive) // admin only
// router.get('/', authorize(Role.Admin), getAll); // admin only
router.get('/:id', authorize(), getById) // all authenticated users
router.get('/', getAll) // admin only
module.exports = router

function update(req, res, next) {
	objService
		.updateById(req.body)
		.then((obj) => {
			obj
				? res.json(obj)
				: res.status(400).json({ message: 'Something went wrong!' })
		})
		.catch((err) => next(err))
}

function del(req, res, next) {
	objService
		.del(req.body)
		.then((obj) => {
			obj
				? res.json(obj)
				: res.status(400).json({ message: 'Something went wrong!' })
		})
		.catch((err) => next(err))
}

function getAll(req, res, next) {
	objService
		.getAll()
		.then((users) => res.json(users))
		.catch((err) => next(err))
}

function getAllActive(req, res, next) {
	objService
		.getAllActive()
		.then((users) => res.json(users))
		.catch((err) => next(err))
}

function getById(req, res, next) {
	const currentUser = req.user
	const id = parseInt(req.params.id)

	// only allow admins to access other user records
	if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
		return res.status(401).json({ message: 'Unauthorized' })
	}

	objService
		.getById(req.params.id)
		.then((user) => (user ? res.json(user) : res.sendStatus(404)))
		.catch((err) => next(err))
}

function add(req, res, next) {
	objService
		.add(req.body.data)
		.then((newobj) => {
			res.json(newobj)
		})
		.catch((err) => next(err))
}
