﻿const models = require('../_helpers/models')

const objects = models.object

module.exports = {
	getAll,
	getAllActive,
	getById,
	updateById,
	del,
	add,
}

async function add(body) {
	return objects.create(body).then((newobj) => {
		return newobj
	})
}

async function del(body) {
	let { id, status } = body.data
	status = !status
	return objects
		.findByPk(id)
		.then((Obj) => {
			if (Obj) {
				return Obj.update({ status })
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function updateById(body) {
	const { id, ...values } = body.data
	return objects
		.findByPk(id)
		.then((Obj) => {
			if (Obj) {
				return Obj.update(values)
					.then((updated) => {
						return updated
					})
					.catch((err) => console.log(err))
			}
		})
		.catch((err) => console.log(err))
}

async function getAllActive() {
	return objects.findAll({
		where: {
			status: 1,
		},
	})
}

async function getAll() {
	return objects.findAll()
}

async function getById(id) {
	const obj = objects.findOne({ where: { id: id } }).then((cb) => {
		const Obj = cb.get()
		if (!Obj) return
		return Obj
	})
	return obj
}
