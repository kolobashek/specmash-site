﻿const express = require('express')
const router = express.Router()
const userService = require('./user.service')
const authorize = require('../_helpers/authorize')
const Role = require('../_helpers/role')

// routes
router.post('/authenticate', authenticate) // public route
router.get('/', authorize(Role.Admin), getAll) // admin only
router.post('/update', authorize(Role.Admin), update)
router.post('/register', register)
router.get('/:id', authorize(), getById) // all authenticated users
module.exports = router

function authenticate(req, res, next) {
	userService
		.authenticate(req.body)
		.then((user) => {
			user
				? res.json(user)
				: res
						.status(400)
						.json({ message: 'Username or password is incorrect' })
		})
		.catch((err) => next(err))
}

function register(req, res, next) {
	console.log(req.body)
	userService
		.register(req.body)
		.then((user) => {
			user
				? !user.message
					? res.json(user)
					: res.status(400).json({ message: user.message })
				: res.status(400).json({ message: 'register ne poshlo' })
		})
		.catch((err) => next(err))
}

function update(req, res, next) {
	userService
		.updateById(req.body)
		.then((user) => {
			console.log(user)
			user
				? !user.message
					? res.json(user)
					: res.status(400).json({ message: user.message })
				: res.status(400).json({ message: 'register ne poshlo' })
		})
		.catch((err) => next(err))
}

function getAll(req, res, next) {
	userService
		.getAll()
		.then((users) => res.json(users))
		.catch((err) => next(err))
}

function getById(req, res, next) {
	const currentUser = req.user
	const id = parseInt(req.params.id)

	// only allow admins to access other user records
	if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
		return res.status(401).json({ message: 'Unauthorized' })
	}

	userService
		.getById(req.params.id)
		.then((user) => (user ? res.json(user) : res.sendStatus(404)))
		.catch((err) => next(err))
}
