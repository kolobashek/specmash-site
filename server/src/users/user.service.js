﻿const config = require('config.json')
const jwt = require('jsonwebtoken')
const Role = require('../_helpers/role')
const models = require('../_helpers/models')
const bCrypt = require('bcrypt-nodejs')

// users hardcoded for simplicity, store in a db for production applications
// const users = [
//     { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
//     { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User }
// ];
const users = models.user

module.exports = {
	authenticate,
	getAll,
	getById,
	register,
	updateById,
}

async function register(body) {
	const hash = bCrypt.hashSync(body.password)
	return users
		.findOne({ where: { username: body.username } })
		.then((user) => {
			if (user) {
				return { message: 'Такой пользователь уже существует' }
			} else {
				return users
					.create({
						username: body.username,
						password: hash,
						phone: body.phone,
						firstname: body.firstname,
						lastname: body.lastname,
						email: body.email,
					})
					.then((user) => {
						const User = user.get()
						const token = jwt.sign(
							{ sub: user.id, role: user.role },
							config.secret
						)
						const { password, ...userWithoutPassword } = User
						return {
							...userWithoutPassword,
							token,
							message:
								'Пользователь успешно создан, уточните подтверждение вашей учетной записи',
						}
					})
			}
		})
}

async function authenticate({ username, password }) {
	const isValidPassword = function (userpass, pass) {
		return bCrypt.compareSync(pass, userpass)
	}
	const user = users
		.findOne({ where: { username: username } })
		.then((user) => {
			if (!user) {
				return null
			}
			if (!isValidPassword(user.password, password)) {
				return null
			} else {
				const User = user.get()
				// const token = jwt.sign({ sub: user.id, role: user.role }, config.secret, { algorithm: 'RS256'})
				const token = jwt.sign(
					{ sub: user.id, role: user.role },
					config.secret
				)
				const { password, ...userWithoutPassword } = User
				return {
					...userWithoutPassword,
					token,
				}
			}
		})
	return user
}

async function updateById(body) {
	const { id, ...values } = body.data
	try {
		const dbUser = await users.findByPk(id)
		const userPromise = await dbUser.update(values)
		const user = userPromise.get()
		const { password, ...userWithoutPassword } = user
		return userWithoutPassword
	} catch (error) {
		return { message: error }
	}
}

async function getAll() {
	return users.findAll()
}

async function getById(id) {
	const user = users.findOne({ where: { id: id } }).then((cb) => {
		const User = cb.get()
		if (!User) return
		const { password, ...userWithoutPassword } = User
		return userWithoutPassword
	})
	return user
}
