﻿// const config = require('config.json')
// const jwt = require('jsonwebtoken')
// const Role = require('../_helpers/role')
const models = require('../_helpers/models')
// const bCrypt = require('bcrypt-nodejs')

// users hardcoded for simplicity, store in a db for production applications
// const users = [
//     { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
//     { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User }
// ];
const shifts = models.shift

module.exports = {
	getAll,
	getById,
	updateById,
	del,
	add,
}

async function add(body) {
	body.date = body.date / 1000
	const shift = await shifts.create(body).then((newshift) => {
		newshift.get()
		newshift.date = newshift.date * 1000
		return newshift
	})
	return shift
}

async function del(body) {
	try {
		return await shifts.destroy({
			where: {
				id: body,
			},
		})
	} catch (err) {
		return { message: err }
	}
}

async function updateById(body) {
	try {
		body.date = body.date / 1000
		const { id, ...values } = body
		const request = await shifts.findByPk(id)
		const shiftReq = await request.update(values)
		const shift = shiftReq.get()
		shift.date = shift.date * 1000
		return shift
	} catch (error) {
		return { message: error }
	}
}

async function getAll(data) {
	try {
		const { gte, lte } = models.Sequelize.Op
		const shiftsPromise = await shifts.findAll({
			where: {
				date: {
					[gte]: data.dateStart / 1000,
					[lte]: data.dateEnd / 1000,
				},
			},
		})
		const allShiftsTimed = shiftsPromise.map((shift) => {
			shift.date = shift.date * 1000
			return shift
		})
		return allShiftsTimed
	} catch (err) {
		return { message: err }
	}
}

async function getById(id) {
	const shift = await shifts.findOne({ where: { id: id } }).then((cb) => {
		const Shift = cb.get()
		if (!Shift) return
		return Shift
	})
	return shift
}
