const express = require('express')
const router = express.Router()
const shiftService = require('./shift.service')
const authorize = require('../_helpers/authorize')
const Role = require('../_helpers/role')

// routes
router.post('/update', authorize(Role.Admin), update) // public route
router.post('/add', authorize(Role.Admin), add)
router.post('/delete', authorize(Role.Admin), del)
// router.get('/', authorize(Role.Admin), getAll); // admin only
router.get('/:id', authorize(), getById) // all authenticated users
router.post('/', getAll) // admin only
module.exports = router

function update(req, res, next) {
	shiftService
		.updateById(req.body.data)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({
					message: unit.message || 'Something went wrong!',
				})
		})
		.catch((err) => next(err))
}

function del(req, res, next) {
	shiftService
		.del(req.body.data)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({
					message: unit.message || 'Something went wrong!',
				})
		})
		.catch((err) => next(err))
}

function getAll(req, res, next) {
	shiftService
		.getAll(req.body.data)
		.then((shifts) => {
			shifts
				? res.json(shifts)
				: res.status(400).json({
					message: shifts.message || 'Something went wrong!',
				})
		})
		.catch((err) => next(err))
}

function getById(req, res, next) {
	const currentUser = req.user
	const id = parseInt(req.params.id)

	// only allow admins to access other user records
	if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
		return res.status(401).json({ message: 'Unauthorized' })
	}

	shiftService
		.getById(req.params.id)
		.then((user) => (user ? res.json(user) : res.sendStatus(404)))
		.catch((err) => next(err))
}

function add(req, res, next) {
	shiftService
		.add(req.body.data)
		.then((newshift) => {
			newshift
				? res.json(newshift)
				: res.status(401).json({ message: 'Failed!' })
		})
		.catch((err) => next(err))
}
