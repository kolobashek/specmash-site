﻿const models = require('../_helpers/models')

// users hardcoded for simplicity, store in a db for production applications
// const users = [
//     { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
//     { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User }
// ];
const advtechs = models.advtech

module.exports = {
	getAll,
	getById,
	updateById,
	del,
	add,
}

async function add(body) {
	console.log('123')
	body.date = body.date / 1000
	const advtech = await advtechs.create(body).then((newadvtech) => {
		console.log(newadvtech)
		newadvtech.get()
		return newadvtech
	})
	return advtech
}

async function del(body) {
	try {
		return await advtechs.destroy({
			where: {
				id: body,
			},
		})
	} catch (err) {
		return { message: err }
	}
}

async function updateById(body) {
	try {
		body.date = body.date / 1000
		const { id, ...values } = body
		const request = await advtechs.findByPk(id)
		const advtechReq = await request.update(values)
		const advtech = advtechReq.get()
		return advtech
	} catch (error) {
		return { message: error }
	}
}

async function getAll() {
	try {
		const advtechsPromise = await advtechs.findAll()
		const allAdvtechsTimed = advtechsPromise.map((advtech) => {
			return advtech
		})
		return allAdvtechsTimed
	} catch (err) {
		return { message: err }
	}
}

async function getById(id) {
	const advtech = await advtechs.findOne({ where: { id: id } }).then((cb) => {
		const Advtech = cb.get()
		if (!Advtech) return
		return Advtech
	})
	return advtech
}
