const express = require('express')
const router = express.Router()
const advtechService = require('./advtech.service')
const authorize = require('../_helpers/authorize')
const Role = require('../_helpers/role')

// routes
router.post('/update', authorize(Role.Admin), update) // public route
router.post('/add', authorize(Role.Admin), add)
router.post('/delete', authorize(Role.Admin), del)
// router.get('/', authorize(Role.Admin), getAll); // admin only
router.get('/:id', getById) // all authenticated users
router.post('/', getAll) // admin only
module.exports = router

function update(req, res, next) {
	advtechService
		.updateById(req.body.data)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({
						message: unit.message || 'Something went wrong!',
				  })
		})
		.catch((err) => next(err))
}

function del(req, res, next) {
	advtechService
		.del(req.body.data)
		.then((unit) => {
			unit
				? res.json(unit)
				: res.status(400).json({
						message: unit.message || 'Something went wrong!',
				  })
		})
		.catch((err) => next(err))
}

function getAll(req, res, next) {
	advtechService
		.getAll(req.body.data)
		.then((advtechs) => {
			advtechs
				? res.json(advtechs)
				: res.status(400).json({
						message: advtechs.message || 'Something went wrong!',
				  })
		})
		.catch((err) => next(err))
}

function getById(req, res, next) {
	advtechService
		.getById(req.params.id)
		.then((advtech) => (advtech ? res.json(advtech) : res.sendStatus(404)))
		.catch((err) => next(err))
}

function add(req, res, next) {
	advtechService
		.add(req.body.data)
		.then((newadvtech) => {
			newadvtech
				? res.json(newadvtech)
				: res.status(401).json({ message: 'Failed!' })
		})
		.catch((err) => next(err))
}
